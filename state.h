// state.h

#ifndef __STATE_H__
#define __STATE_H__

// Using a template to make the framework transferrable
template <class T>
class State
{
	public:
		virtual void Enter( T* )=0;
		virtual void Execute( T* )=0;
		virtual void Exit( T* )=0;
};

#endif