#include "FuzzyLogic.h"

// Fuzzy Base Functions

double FuzzyBase::Downslope( double x, double l, double r )
{
	return ( ( r - x ) / ( r - l ) );
}

double FuzzyBase::Upslope( double x, double l, double r )
{
	return ( ( x - l ) / ( r - l ) );
}

double FuzzyBase::Not( double a )
{
	return 1.0 - a;
}

double FuzzyBase::And( double lhs, double rhs )
{
	return lhs <= rhs ? lhs : rhs;
}

double FuzzyBase::Or( double lhs, double rhs )
{
	return lhs >= rhs ? lhs : rhs;
}

double FuzzyBase::Extend( double a )
{
	return a < 0.5 ? 0.0 : 1.0;
}


// Fuzzy Health Functions

double FuzzyHealth::GetLowMembership( double x )
{
	if( x <= H_LOW_LOWER )
		return 1.0;
	else if( x > H_LOW_UPPER )
		return 0.0;
	else
		return Downslope( x, H_LOW_LOWER, H_LOW_UPPER );
}

double FuzzyHealth::GetMediumMembership( double x )
{
	if( x <= H_MED_LOWER )
	{
		return 0.0;
	}
	else if( x > H_MED_LOWER && x < H_MED_PLAT_L )
	{
		 return Upslope( x, H_MED_LOWER, H_MED_PLAT_L );
	}
	else if( x > H_MED_PLAT_R && x < H_MED_UPPER )
	{
		return Downslope( x, H_MED_PLAT_R, H_MED_UPPER );
	}
	else if( x > H_MED_UPPER )
	{
		return 0.0;
	}
	else
	{
		return 1.0;
	}
}

double FuzzyHealth::GetHighMembership( double x )
{
	if( x >= H_HIGH_UPPER )
		return 1.0;
	else if( x <= H_HIGH_LOWER )
		return 0.0;
	else
		return Upslope( x, H_HIGH_LOWER, H_HIGH_UPPER );
}


// Fuzzy Range Functions
double FuzzyRange::GetCloseMembership( double x )
{
	if( x <= R_CLOSE_LOWER )
		return 1.0;
	else if( x > R_CLOSE_UPPER )
		return 0.0;
	else
		return Downslope( x, R_CLOSE_LOWER, R_CLOSE_UPPER );
}

double FuzzyRange::GetMediumMembership( double x )
{
	if( x <= R_MED_LOWER )
	{
		return 0.0;
	}
	else if( x > R_MED_LOWER && x < R_MED_PLAT_L )
	{
		return Upslope( x, R_MED_LOWER, R_MED_PLAT_L );
	}
	else if( x > R_MED_PLAT_R && x < R_MED_UPPER )
	{
		return Downslope( x, R_MED_PLAT_R, R_MED_UPPER );
	}
	else if( x > R_MED_UPPER )
	{
		return 0.0;
	}
	else
	{
		return 1.0;
	}
}

double FuzzyRange::GetFarMembership( double x )
{
	if( x >= R_FAR_UPPER )
		return 1.0;
	else if( x <= R_FAR_LOWER )
		return 0.0;
	else
		return Upslope( x, R_FAR_LOWER, R_FAR_UPPER );
}


// Fuzzy Score Functions
double FuzzyScore::GetSmallMembership( double x )
{
	if( x <= S_SMALL_LOWER )
		return 1.0;
	else if( x > S_SMALL_UPPER )
		return 0.0;
	else
		return Downslope( x, S_SMALL_LOWER, S_SMALL_UPPER );
}

double FuzzyScore::GetLargeMembership( double x )
{
	if( x >= S_LARGE_UPPER )
		return 1.0;
	else if( x <= S_LARGE_LOWER )
		return 0.0;
	else
		return Upslope( x, S_LARGE_LOWER, S_LARGE_UPPER );
}


// Fuzzy Accuracy Functions
double FuzzyAccuracy::GetLowMembership( double x )
{
	if( x <= A_LOW_LOWER )
		return 1.0;
	else if( x > A_LOW_UPPER )
		return 0.0;
	else
		return Downslope( x, A_LOW_LOWER, A_LOW_UPPER );
}

double FuzzyAccuracy::GetMediumMembership( double x )
{
	if( x <= A_MED_LOWER )
		return 0.0;
	else if( x > A_MED_LOWER && x < A_MED_PLAT_L )
		return Upslope( x, A_MED_LOWER, A_MED_PLAT_L );
	else if( x > A_MED_PLAT_R && x < A_MED_UPPER )
		return Downslope( x, A_MED_PLAT_R, A_MED_UPPER );
	else
		return 1.0;
}

double FuzzyAccuracy::GetHighMembership( double x )
{
	if( x >= A_HIGH_UPPER )
		return 1.0;
	else if( x <= A_HIGH_LOWER )
		return 0.0;
	else
		return Upslope( x, A_HIGH_LOWER, A_HIGH_UPPER );
}