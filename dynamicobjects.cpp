#include "dynamicobjects.h"
//#include "renderer.h"
#include "errorlogger.h"

#define NULL 0


DynamicObjects::DynamicObjects()
{
	m_iNumPlacedDominationPoints =0;
	// Tell each bot its own team and number
	// (I hate this)
	for(int i=0;i<NUMTEAMS;i++)
	{
		for(int f=0;f<NUMBOTSPERTEAM;f++)
		{
			m_rgTeams[i].m_rgBots[f].SetOwnNumbers(i,f);
		}
	}

}

void DynamicObjects::Initialise()
{
	m_dNextScorePoint=0.0;

	for(int i=0;i<NUMTEAMS;i++)
	{
		for(int f=0;f<NUMBOTSPERTEAM;f++)
		{
			m_rgTeams[i].m_rgBots[f].StartAI();	
		}
	}
}

DynamicObjects::~DynamicObjects()
{

}





DynamicObjects* DynamicObjects::pInst = NULL;

DynamicObjects* DynamicObjects::GetInstance()
{
	if(pInst == NULL)
	{
		pInst= new DynamicObjects();
	}

	return pInst;
}

void DynamicObjects::Release()		// Static
{
	if(pInst)
	{
		delete pInst;
		pInst = NULL;
	}
}

ErrorType DynamicObjects::Update(double frametime)
{
	// Update all bots
	for(int i=0;i<NUMTEAMS;i++)
	{
		for(int f=0;f<NUMBOTSPERTEAM;f++)
		{
			m_rgTeams[i].m_rgBots[f].Update(frametime);
		}
	}

	// Check for alliance changes to Domination points
	for(int i=0;i<NUMDOMINATIONPOINTS;i++)
	{
		int numTeamsInRange =0;
		int lastTeamFound = -1;

		for(int f=0;f<NUMTEAMS;f++)
		{	
			for(int g=0;g<NUMBOTSPERTEAM;g++)
			{		
				double range = (m_rgDominationPoints[i].m_Location - m_rgTeams[f].m_rgBots[g].GetLocation()).magnitude();
				if(range<DOMINATIONRANGE && m_rgTeams[f].m_rgBots[g].IsAlive())
				{
					numTeamsInRange++;
					lastTeamFound=f;
					break;
				}
			}
		}
		if(numTeamsInRange==1)
		{
			if(m_rgDominationPoints[i].m_OwnerTeamNumber != lastTeamFound)
			{
				Renderer::GetInstance()->ShowDominationPointClaimed(lastTeamFound);
				m_rgDominationPoints[i].m_OwnerTeamNumber = lastTeamFound;
			}
		}
	}

	// Add scores for domination points
	m_dNextScorePoint+=frametime;
	if(m_dNextScorePoint>1.0)
	{
		for(int i=0;i<NUMDOMINATIONPOINTS;i++)
		{	
			int team = m_rgDominationPoints[i].m_OwnerTeamNumber;
			if(team>=0 )
			{
				m_rgTeams[team].m_iScore++;
			}
		}
		m_dNextScorePoint-=1.0;
	}

	SendData d;
	PackData( d );
	NetworkManager::GetInstance()->UpdateData( d );


	return SUCCESS;
}

int DynamicObjects::GetScore(int teamNumber)
{
	return m_rgTeams[teamNumber].m_iScore;
}

ErrorType DynamicObjects::Render()
{
	ErrorType answer = SUCCESS;

	Renderer* pRenderer = Renderer::GetInstance();
	for(int i=0;i<NUMTEAMS;i++)
	{
		for(int f=0;f<NUMBOTSPERTEAM;f++)
		{
			Bot& currentBot = m_rgTeams[i].m_rgBots[f];
			if(currentBot.IsAlive())
			{
				if(pRenderer->DrawBot(currentBot.GetLocation(), currentBot.GetDirection() , i) == FAILURE)
				{
					ErrorLogger::Writeln("Renderer failed to draw a bot");
					answer=FAILURE;
				}
			}
			else	// Current Bot is dead
			{
				if(pRenderer->DrawDeadBot(currentBot.GetLocation() , i) == FAILURE)
				{
					ErrorLogger::Writeln("Renderer failed to draw a dead bot");
					// Non critical error
				}
			}
		}
	}

	for(int i=0;i<NUMDOMINATIONPOINTS;i++)
	{
		if(pRenderer->DrawDominationPoint( m_rgDominationPoints[i].m_Location, m_rgDominationPoints[i].m_OwnerTeamNumber)==FAILURE)
		{
				ErrorLogger::Writeln("Renderer failed to draw a domination point");
				answer=FAILURE;
		}
	}

	return SUCCESS;
}


Bot& DynamicObjects::GetBot(int teamNumber, int botNumber)
{
	teamNumber= teamNumber%NUMTEAMS;
	botNumber= botNumber%NUMBOTSPERTEAM;
	return m_rgTeams[teamNumber].m_rgBots[botNumber];
}

DominationPoint DynamicObjects::GetDominationPoint(int pointNumber)
{
	return m_rgDominationPoints[pointNumber];
}

void DynamicObjects::PlaceDominationPoint(Vector2D location)
{
	if(m_iNumPlacedDominationPoints<NUMDOMINATIONPOINTS)
	{
		m_rgDominationPoints[m_iNumPlacedDominationPoints].m_Location=location;
		m_rgDominationPoints[m_iNumPlacedDominationPoints].m_OwnerTeamNumber=-1;
		++m_iNumPlacedDominationPoints;
	}
}

int DynamicObjects::GetNumPlacedDomPoints() const
{
	return m_iNumPlacedDominationPoints;
}

int DynamicObjects::GetNumCapturePoints( int teamNumber ) const
{
	int count = 0;

	for( int i=0; i < NUMDOMINATIONPOINTS; i++ )
	{
		if( m_rgDominationPoints[i].m_OwnerTeamNumber == teamNumber )
			count++;
	}

	return count;
}

void DynamicObjects::PackData( SendData &sd )
{
	for( int i=0; i < NUMDOMINATIONPOINTS; i++ )
	{
		sd.doms[i].owner = m_rgDominationPoints[i].m_OwnerTeamNumber;
	}

	for( int i=0; i < NUMTEAMS; i++ )
	{
		sd.teams[i].score = m_rgTeams[i].m_iScore;

		
		for( int j=0; j < NUMBOTSPERTEAM; j++ )
		{
			sd.teams[i].bots[j].x         = m_rgTeams[i].m_rgBots[j].GetLocation().XValue;
			sd.teams[i].bots[j].y         = m_rgTeams[i].m_rgBots[j].GetLocation().YValue;
			sd.teams[i].bots[j].respawn   = m_rgTeams[i].m_rgBots[j].GetRespawnTime();
			sd.teams[i].bots[j].direction = m_rgTeams[i].m_rgBots[j].GetDirection();
			sd.teams[i].bots[j].tX        = m_rgTeams[i].m_rgBots[j].GetShotTarget().XValue;
			sd.teams[i].bots[j].tY        = m_rgTeams[i].m_rgBots[j].GetShotTarget().YValue;
			sd.teams[i].bots[j].shooting  = m_rgTeams[i].m_rgBots[j].SendShot();
			sd.teams[i].bots[j].damage    = m_rgTeams[i].m_rgBots[j].GetDamageDone();
			
			if( m_rgTeams[i].m_rgBots[j].SendShot() )
				m_rgTeams[i].m_rgBots[j].SetSendShot( false );
		}
	}
}
