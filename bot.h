#pragma once

#include "shapes.h"
#include "rules.h"
#include "behaviours.h"
#include <vector>
#include "StateMachine.h"
#include <string>

const int NUM_SENSORS = 5;
const int SENSOR_LEN  = 25;

class Bot
{
protected:
	Vector2D m_Position;			// Current world coordinates
	Vector2D m_Velocity;			// Current velocity
	Vector2D m_Acceleration;
	double m_dDirection;			// Direction the bot is pointing in. radians anticlockwise from south
//	int m_iAmmo;					// Not currently used
	bool m_bFiring;
	bool m_bAiming;					// If true, bot is aiming and cannot move
	double m_dTimeToCoolDown;		// Countdown until the time the bot can shoot again
	double m_dTimeToRespawn;		// Countdown until the bot can respawn. If zero or below, bot is alive
	int m_iAimingAtTeam;			// Team number of the bot being aimed at
	int m_iAimingAtBot;				// Number of the bot being aimed at
	int m_iOwnTeamNumber;
	int m_iOwnBotNumber;
	double m_dAccuracy;				// Accuracy of the current firing solution (1==100%)
	int m_iHealth;					// Health (up to 100)
	Behaviours behaviours;
	bool m_drawSensors;
	int m_currentDPTarget; // Current DP target
	int m_nearestEnemy;    // Nearest enemy at last check
	bool m_initialSpawn;
	Vector2D shotTarget;
	bool sendShot;
	int m_damage;

	StateMachine<Bot> *m_stateMachine;

	// A little hacky, but I'm friending the substates
	// so they can access behaviours
	friend class Flee;
	friend class CaptureDomPoint;
	friend class Attack;
	friend class Defend;
	friend class Spawn;

	friend class FuzzyFlee;
	friend class FuzzyCaptureDomPoint;
	friend class FuzzyAttack;
	friend class FuzzyDefend;
	friend class FuzzySpawn;

public:
	Vector2D m_sensors[NUM_SENSORS];
	Bot();	
	~Bot();
	// Runs once each frame. Handles physics, shooting, and calls
	// ProcessAI
	void Update(double frametime);

	// Returns the location of the bot
	Vector2D GetLocation();

	// Returns the velocity of the bot
	Vector2D GetVelocity();
	
	// Returns the direction the bot is pointing. In radians anticlockwise from south
	double GetDirection();

	// Restarts the bot in a new location, with full heath, etc
	void PlaceAt(Vector2D position);	
	
	// Returns true if the bot is currently not respawning
	bool IsAlive();					
	
	// This is your function. Use it to set up any states at the beginning of the game
	// and analyse the map.
	// Remember that bots have not spawned yet, so will not be in their
	// starting positions.
	void StartAI();

	// This is your function. Use it to set the orders for the bot.
	// Will be called once each frame from Update
	void ProcessAI();			
			
	// Returns the number of the team of the bot being aimed at.
	// Returns a negative number if no bot is being aimed at.
	int GetTargetTeam();	

	// Returns the number of the bot being aimed at.
	// Returns a negative number if no bot is being aimed at.
	int GetTargetBot();

	// Sets the bots own team number and bot number.
	// No need to call this
	void SetOwnNumbers(int teamNo, int botNo);

	// Returns the current health of the bot
	int GetHealth();

	// Returns the current accuracy of the bot.
	// Accuracy is the probability of hitting the current target.
	// If the bot is not aiming, this will be zero.
	double GetAccuracy();

	// Sets the target of the current bot.
	// This will reset the accuracy to zero if the target bot 
	// has changed.
	// It also sets the bot to aiming mode, which means it will stop
	// moving.
	// If you want the bot to stop aiming, just set mbAiming = false
	// or use the StopAiming method
	void SetTarget(int targetTeamNo, int targetBotNo);

	// Call this to set the bot to shoot, if it can.
	// Once a shot it taken, the bot will not shoot again unless told to do so.
	void Shoot();

	// Delivers the specified damage to the bot.
	// If this drops health below zero, the bot will die
	// and will respawn at the spawn point after a specified interval
	void TakeDamage(int amount);

	// Stops the bot from aiming, so it can move again
	void StopAiming();

	// Set the current path to follow
	void SetPath( Vector2D _target );

	Bot* FindMostDangerousEnemy();

	int GetTeamNumber() const;

	int GetBotNumber() const;

	void DrawBotNumber() const;

	StateMachine<Bot>* GetFSM() const;

	void SetCurrentDPTarget( int _dp );

	int GetCurrentDPTarget() const;

	void SetNearestEnemy( int _enemy );

	int GetNearestEnemy() const;

	void UpdateSensors();

	int GetResponseTime() const;

	double GetRespawnTime() const;

	bool InitialSpawn() const;

	void DeactivateInitialSpawn();

	bool SendShot() const;

	void SetSendShot( bool b );

	Vector2D GetShotTarget() const;

	int GetDamageDone() const;

	double GetCooldown() const;
};
