#pragma once
#include "shapes.h"
#include "ErrorType.h"
#include "soundplayer.h"


struct Shot
{
	static const double LIFETIME;
	Vector2D from;
	Vector2D to;
	double time;

	Shot(): time(0)
	{}
};

struct Blood
{
	static const double LIFETIME;
	Vector2D m_location;
	Vector2D velocity;
	double time;
	Blood(): time(0)
	{}
};

class Renderer
{
private:
	static const int NUMBLOOD=500;
	static const int NUMSHOTS=10;
	Renderer();
	~Renderer();
	Renderer(const Renderer& other);		// Disabled
	static Renderer* pInst;
	int m_iNextBlood;						// Next blood element to use
	Blood m_rgBlood[NUMBLOOD];	
	int m_iNextShot;						// Next shot element to use
	Shot m_rgShot[NUMSHOTS];

	Vector2D m_ScreenDimension;
	Vector2D m_Centre;
	double m_dScale;

	Vector2D GetTopLeft()
	{
		return (m_Centre - m_ScreenDimension/2*m_dScale);
	}


	SoundPlayer m_SoundFX;
	int GetTeamColour(int teamNumber);

public:
	Vector2D ScreenPosition(Vector2D location) // Temp moved from private
	{
		return (location-GetTopLeft())/m_dScale;
	}
	static Renderer* GetInstance();				// Thread-safe singleton
	static void Release();
	void SetViewCentre(Vector2D centre);
	void SetViewScale(double scale);
	ErrorType DrawBlock(Rectangle2D area);
	ErrorType DrawDominationPoint(Vector2D location, int teamNumber);
	ErrorType DrawBot(Vector2D location, double direction, int teamNumber);
	ErrorType DrawDeadBot(Vector2D location, int teamNumber);
	ErrorType DrawMousePointer(Vector2D location);
	ErrorType DrawFlagPoint(Vector2D location, int teamNumber);
	ErrorType AddShot(Vector2D from, Vector2D to);
	ErrorType AddBloodSpray(Vector2D location, Vector2D direction, int size);
	ErrorType DrawFX(double frametime);
	ErrorType DrawTextAt(Vector2D position, char* text);
	ErrorType DrawTextAt(Vector2D position, char* text, int r, int b, int g);
	ErrorType DrawNumberAt(Vector2D position, double number);
	ErrorType DrawLine(Vector2D from, Vector2D to, int teamNumber=-1);
	ErrorType DrawDot(Vector2D position, int teamNumber=-1);
	ErrorType ShowDominationPointClaimed(int teamNumber);

	// Flips and clears the back buffer
	ErrorType EndScene();

	// Returns success if the back buffer is valid (i.e. not minimised or something)
	// Returns failure otherwise
	ErrorType GetIsValid();
};
