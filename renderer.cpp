#include "renderer.h"
#include "mydrawengine.h"
#define NULL 0

const double Shot::LIFETIME=0.1;
const double Blood::LIFETIME=1.0;

Renderer::Renderer()
{
	m_Centre.set(0,0);
	m_ScreenDimension.set( MyDrawEngine::GetInstance()->screenWidth, 
											MyDrawEngine::GetInstance()->screenHeight);
	m_dScale=1.0;
	m_iNextBlood=0;						// Next blood element to use
	m_iNextShot=0;
	m_SoundFX.LoadAssets();
}

Renderer::~Renderer()
{

}



Renderer* Renderer::pInst=NULL;

Renderer* Renderer::GetInstance()				// Static
{
	if(pInst == NULL)
	{
		pInst= new Renderer();
	}

	return pInst;
}

void Renderer::Release()
{
	pInst->m_SoundFX.Release();
	if(pInst)
	{
		delete pInst;
		pInst = NULL;
	}
}

ErrorType Renderer::ShowDominationPointClaimed(int teamNumber)
{
	return(m_SoundFX.PlayDomClaim());
}

void Renderer::SetViewCentre(Vector2D centre)
{
	this->m_Centre=centre;
}
void Renderer::SetViewScale(double scale)
{
	this->m_dScale=scale;
}

//void Renderer::SetCurrentViewpoint(Viewpoint v)
//{
//	mCurrentViewpoint = v;
//	mCurrentViewpoint.screenDimension.set(MyDrawEngine::GetInstance()->screenWidth, 
//											MyDrawEngine::GetInstance()->screenHeight);
//}

ErrorType Renderer::DrawBlock(Rectangle2D area)
{
	Vector2D tl = ScreenPosition(area.GetTopLeft());
	Vector2D br = ScreenPosition(area.GetBottomRight());
	RECT destRect;
	destRect.top	= int(tl.YValue);
	destRect.left	= int(tl.XValue);
	destRect.bottom	= int(br.YValue);
	destRect.right	= int(br.XValue);

	return MyDrawEngine::GetInstance()->FillRect(destRect, MyDrawEngine::WHITE32);
}

int Renderer::GetTeamColour(int teamNumber)
{
	int answer;
	switch(teamNumber)
	{
	case 0:
		answer = MyDrawEngine::GetInstance()->RED32;
		break;
	case 1:
		answer = MyDrawEngine::GetInstance()->BLUE32;
		break;
	case 2:
		answer = MyDrawEngine::GetInstance()->GREEN32;
		break;
	case 3:
		answer = MyDrawEngine::GetInstance()->PURPLE32;
		break;
	case 4:
		answer = MyDrawEngine::GetInstance()->CYAN32;
		break;
	case 5:
		answer = MyDrawEngine::GetInstance()->YELLOW32;
		break;
	case 6:
		answer = MyDrawEngine::GetInstance()->WHITE32;
		break;
	case 7:
		answer = MyDrawEngine::GetInstance()->LIGHTRED32;
		break;
	default:
		answer = MyDrawEngine::GetInstance()->GREY32;
		break;	

	}
	return answer;
}

ErrorType Renderer::DrawMousePointer(Vector2D location)
{
	Vector2D pos = ScreenPosition(location);

	RECT destRect1;
	destRect1.top	= int(pos.YValue-8);
	destRect1.left	= int(pos.XValue-2);
	destRect1.bottom	= int(pos.YValue+9);
	destRect1.right	= int(pos.XValue+3);

	RECT destRect2;
	destRect2.top	= int(pos.YValue-2);
	destRect2.left	= int(pos.XValue-8);
	destRect2.bottom	= int(pos.YValue+3);
	destRect2.right	= int(pos.XValue+9);

	MyDrawEngine::GetInstance()->FillRect(destRect1, MyDrawEngine::GREY32);
	return MyDrawEngine::GetInstance()->FillRect(destRect2, MyDrawEngine::GREY32);


}

ErrorType Renderer::DrawDominationPoint(Vector2D location, int teamNumber)
{
	Vector2D pos = ScreenPosition(location);

	return MyDrawEngine::GetInstance()->FillCircle(pos, int(6/m_dScale), GetTeamColour(teamNumber));
}

ErrorType Renderer::DrawBot(Vector2D location, double direction, int teamNumber)
{
	ErrorType answer = SUCCESS;

	Vector2D pos = ScreenPosition(location);

	answer = MyDrawEngine::GetInstance()->FillCircle(pos, int(8/m_dScale), GetTeamColour(teamNumber));
	if(MyDrawEngine::GetInstance()->FillCircle(pos, int(6/m_dScale), 0)==FAILURE)
		answer=FAILURE;

	Vector2D pointer;
	pointer.setBearing(direction, int(10/m_dScale));
	if(MyDrawEngine::GetInstance()->DrawLine(pos, pos+pointer, GetTeamColour(teamNumber))==FAILURE)
		answer=FAILURE;

	return answer;
}

ErrorType Renderer::DrawTextAt(Vector2D position, char* text)
{
	return MyDrawEngine::GetInstance()->WriteText(int(position.XValue),  int(position.YValue), text);
}

ErrorType Renderer::DrawTextAt(Vector2D position, char* text, int r, int g, int b)
{
	return MyDrawEngine::GetInstance()->WriteText(int(position.XValue),  int(position.YValue), text, r, g, b);
}

ErrorType Renderer::DrawNumberAt(Vector2D position, double number)
{
	return MyDrawEngine::GetInstance()->WriteDouble(number, int(position.XValue),  int(position.YValue));
}

ErrorType Renderer::DrawDeadBot(Vector2D location, int teamNumber)
{
	ErrorType answer = SUCCESS;

	Vector2D pos = ScreenPosition(location);
	Vector2D xoff(int(6/m_dScale), 0);
	Vector2D yoff(0,int(6/m_dScale));

	answer = MyDrawEngine::GetInstance()->DrawLine(pos+xoff+yoff, pos-xoff-yoff, GetTeamColour(teamNumber));
	if(MyDrawEngine::GetInstance()->DrawLine(pos-xoff+yoff, pos+xoff-yoff, GetTeamColour(teamNumber))==FAILURE)
		answer=FAILURE;

	return answer;
}


ErrorType Renderer::DrawFlagPoint(Vector2D location, int teamNumber)
{
	Vector2D pos = ScreenPosition(location);

	return MyDrawEngine::GetInstance()->FillCircle(pos, int(6/m_dScale), GetTeamColour(teamNumber));
}



ErrorType Renderer::AddShot(Vector2D from, Vector2D to)
{
	m_SoundFX.PlayShot();
	m_rgShot[m_iNextShot].from=from;
	m_rgShot[m_iNextShot].to=to;
	m_rgShot[m_iNextShot].time=Shot::LIFETIME;
	++m_iNextShot;
	if(m_iNextShot>=NUMSHOTS)
		m_iNextShot=0;
	return SUCCESS;
}

ErrorType Renderer::DrawLine(Vector2D from, Vector2D to, int teamNumber)
{
	from = ScreenPosition(from);
	to = ScreenPosition(to);
	return MyDrawEngine::GetInstance()->DrawLine(from, to, GetTeamColour(teamNumber));
}

ErrorType Renderer::DrawDot(Vector2D position, int teamNumber)
{
	Vector2D pos = ScreenPosition(position);
	RECT dot;
	dot.left = int(pos.XValue-5/m_dScale);
	dot.right = int(pos.XValue+5/m_dScale);
	dot.top = int(pos.YValue-5/m_dScale);
	dot.bottom = int(pos.YValue+5/m_dScale);

	return MyDrawEngine::GetInstance()->FillRect(dot, GetTeamColour(teamNumber));
}

ErrorType Renderer::DrawFX(double frametime)
{
	// Shots
	for(int i=0;i<NUMSHOTS;++i)
	{
		if(m_rgShot[i].time>0)
		{
			Vector2D scn1 = (ScreenPosition(m_rgShot[i].from));
			Vector2D scn2 = (ScreenPosition(m_rgShot[i].to));
			double shade = m_rgShot[i].time/Shot::LIFETIME;
			int colour = _XRGB(int(255*shade), int(255*shade), int(255*shade));
			MyDrawEngine::GetInstance()->DrawLine(scn1, scn2, colour);
			m_rgShot[i].time-=frametime;	
		}
	}
	// Spray
	int detail = 1;//int(m_dScale)+1;			// Reduces how much blood is drawn at large scales
	for(int i=0;i<NUMBLOOD;i+=detail)
	{
		if(m_rgBlood[i].time>0)
		{
			Vector2D scn = (ScreenPosition(m_rgBlood[i].m_location));
			MyDrawEngine::GetInstance()->DrawPoint(scn, MyDrawEngine::RED32);
			m_rgBlood[i].m_location+=m_rgBlood[i].velocity*frametime;
			m_rgBlood[i].velocity-=m_rgBlood[i].velocity*frametime/Blood::LIFETIME;

			m_rgBlood[i].time-=frametime;	
		}
	}

	return SUCCESS;
}

ErrorType Renderer::GetIsValid()
{
	//  validation routine - should be able to alt-tab out of program
	// (Done in game main just to show that it needs to be done - better to do in MyDrawEngine. really)
	if(MyDrawEngine::GetInstance()->Validate()==FAILURE) return FAILURE;
	else
		return SUCCESS;
}

ErrorType Renderer::EndScene()
{
	//Flip and clear the back buffer
	MyDrawEngine::GetInstance()->Flip();
	MyDrawEngine::GetInstance()->ClearBackBuffer();
	return SUCCESS;
}

ErrorType Renderer::AddBloodSpray(Vector2D location, Vector2D direction, int size)
{
	const double BLOODSPEED = 200;
	direction = direction.unitVector()*BLOODSPEED;
	for(int i=0;i<size;i++)
	{
		Vector2D spray;
		spray.setBearing(rand()*628/100.0, rand()%900/10.0);
		spray+=direction;
		m_rgBlood[m_iNextBlood].m_location=location;
		m_rgBlood[m_iNextBlood].velocity=spray;
		m_rgBlood[m_iNextBlood].time=Blood::LIFETIME;
		++m_iNextBlood;
		if(m_iNextBlood>=NUMBLOOD)
			m_iNextBlood=0;
	}
	return SUCCESS;

}
