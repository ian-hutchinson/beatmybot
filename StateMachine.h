#include "state.h"

template <class T>
class StateMachine
{
private:
	T* m_owner;
	State<T> *m_currentState;
	State<T> *m_previousState;
	State<T> *m_globalState;

public:
	StateMachine( T *_owner ): m_owner( _owner ),
							   m_currentState( NULL ),
							   m_previousState( NULL ),
							   m_globalState( NULL )
	{}

	// Setters
	void SetCurrentState( State<T> * _s ) { m_currentState = _s; m_currentState->Enter( m_owner ); }
	void SetPreviousState( State<T> *_s ) { m_previousState = _s; }
	void SetGlobalState( State<T> *_s ) { m_globalState = _s; }

	void Update() const
	{
		if( m_globalState )
			m_globalState->Execute( m_owner );

		if( m_currentState )
			m_currentState->Execute( m_owner );
	}

	void ChangeState( State<T> *_newState )
	{
		if( !_newState )
		{
			// Trying to assign a null state!
			return;
		}

		if( m_currentState != _newState ) // This line should fix the attack glitch
		{
			// Record previous state
			m_previousState = m_currentState;
		}

		// Exit current state
		m_currentState->Exit( m_owner );

		// Change to new state
		m_currentState = _newState;

		// Enter new state
		m_currentState->Enter( m_owner );

	}

	void RevertState()
	{
		ChangeState( m_previousState );
	}

	// Getters
	State<T>* CurrentState() const { return m_currentState; }
	State<T>* PreviousState() const { return m_previousState; }
	State<T>* GlobalState() const { return m_globalState; }

	bool IsInState( State<T> *_s ) const
	{
		if( m_currentState == _s )
			return true;

		return false;
	}

	bool PreviousStateWas( State<T> *_s ) const
	{
		if( m_previousState == _s )
			return true;

		return false;
	}
};