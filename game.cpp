#include "game.h"
#include "staticmap.h"
#include "Renderer.h"
#include "errorlogger.h"
#include "myinputs.h"
#include "dynamicobjects.h"
#include "networkmanager.h"
#include "NodeGraph.h"
#include "substates.h"


Game* Game::pInst = NULL;

//NodeGraph ng;

Game* Game::GetInstance()
{
	if(pInst == NULL)
	{
		pInst= new Game();
	}

	return pInst;
}


void Game::Release()		// Static
{
	if(pInst)
	{
		delete pInst;
		pInst = NULL;
	}
}


Game::Game()
{
	InitialiseScript();
}

Game::~Game()
{
	
}

ErrorType Game::Start()
{
	m_timer.mark();
	m_timer.mark();
	m_State = RUNNING;



	return SUCCESS;
}

ErrorType Game::RunInterface()
{
	ErrorType answer=SUCCESS;

	Renderer* pTheRenderer = Renderer::GetInstance();

	// Handle mouse
	static int mouseX = 500;
	static int mouseY = 600;
	static int screenCentreX = 500;
	static int screenCentreY = 600;
	static double zoom = 1.0;
	MyInputs* pInputs = MyInputs::GetInstance();
	pInputs->SampleMouse();

	mouseX += int(pInputs->GetMouseDX()*zoom);
	mouseY += int(pInputs->GetMouseDY()*zoom);

	if(mouseX<screenCentreX-200*zoom)
		screenCentreX+=int((mouseX-screenCentreX+200*zoom)/5*zoom);
	if(mouseX>screenCentreX+200*zoom)
		screenCentreX-=int((screenCentreX-mouseX+200*zoom)/5*zoom);
	if(mouseY<screenCentreY-200*zoom)
		screenCentreY+=int((mouseY-screenCentreY+200*zoom)/5*zoom);
	if(mouseY>screenCentreY+200*zoom)
		screenCentreY-=int((screenCentreY-mouseY+200*zoom)/5*zoom);

	// Handle viewport
	pTheRenderer->SetViewScale(zoom);
	pTheRenderer->SetViewCentre(Vector2D(screenCentreX,screenCentreY));

	pTheRenderer->DrawMousePointer( Vector2D(mouseX, mouseY));

	if(pInputs->GetMouseDZ()<0)
		zoom*=1.05;
	if(pInputs->GetMouseDZ()>0)
		zoom/=1.05;

	// Display the scores

	for(int i=0;i<NUMTEAMS;i++)
	{
		int score=(DynamicObjects::GetInstance()->GetScore(i));
		Vector2D pos(10, i*20+10);
		pTheRenderer->DrawTextAt(pos, "Team ");
		pos.set(55, i*20+10);
		pTheRenderer->DrawNumberAt(pos, i+1);
		pos.set(75, i*20+10);
		pTheRenderer->DrawNumberAt(pos, score);
	}

	// Network output
	
	Vector2D netlog_pos( 10, 700 );
	pTheRenderer->DrawTextAt( netlog_pos, "Server status: " );

	NetworkManager *pNetMan = NetworkManager::GetInstance();
	netlog_pos.XValue += 105;

	if( pNetMan )
	{
		pTheRenderer->DrawTextAt( netlog_pos, "Online", 0, 255, 0 );
		netlog_pos.set( 10, 720 );
		pTheRenderer->DrawTextAt( netlog_pos, "Clients: " );
		netlog_pos.XValue += 55;
		pTheRenderer->DrawNumberAt( netlog_pos, pNetMan->GetNumClientConnected() );
	}
	else
		pTheRenderer->DrawTextAt( netlog_pos, "Offline", 255, 0, 0 ); 
	


	return answer;
}

ErrorType Game::Update()
{
	m_timer.mark();
	// Cap the frame time
	if(m_timer.mdFrameTime>0.05)
		m_timer.mdFrameTime=0.05;
	ErrorType answer=SUCCESS;

	Renderer* pTheRenderer = Renderer::GetInstance();

	// If not paused or minimised
	if(m_State == RUNNING)
	// Update Dynamic objects
	{
		DynamicObjects::GetInstance()->Update(m_timer.mdFrameTime);
	}

	// Render
	if(pTheRenderer->GetIsValid()==FAILURE)
	{
		m_State = MINIMISED;			// Or some other reason why invalid
	}
	else
	{
		if(m_State == MINIMISED)
			m_State = PAUSED;			// No longer minimised. Return to pause state.
		if(StaticMap::GetInstance()->Render()==FAILURE)
		{
			ErrorLogger::Writeln("Static map failed to render. Terminating.");
			answer = FAILURE;
		}
		if(DynamicObjects::GetInstance()->Render()==FAILURE)
		{
			ErrorLogger::Writeln("Dynamic objects failed to render. Terminating.");
			answer = FAILURE;
		}

		if(pTheRenderer->DrawFX(m_timer.mdFrameTime)==FAILURE)
		{
			ErrorLogger::Writeln("Render failed to draw FX.");
			// Continue. Non-critical error.
		}
		if(pTheRenderer->EndScene()==FAILURE)
		{
			ErrorLogger::Writeln("Render failed to end scene. Terminating.");
			answer = FAILURE;
		}
	}

	if(RunInterface()==FAILURE)
	{
		ErrorLogger::Writeln("Interface reports failure.");
		// Non-critical error
	}

	// Update network
	//NetworkManager *pNetMan = NetworkManager::GetInstance();

	if( NodeGraph::GetInstance()->DrawModeEnabled() )
		NodeGraph::GetInstance()->DrawAllNodes();

	//ng.DrawPath( testPath );
	// This will all edges between line-of-sight nodes.
	// Warning, it will SERIOUSLY slow things down
	//ng.DrawAllEdges();

	//Renderer::GetInstance()->DrawNumberAt( Vector2D( 500, 700 ), ng.GetNumNodes() );

	return answer;
}

ErrorType Game::End()
{
	Renderer::Release();
	StaticMap::Release();
	DynamicObjects::Release();
	return SUCCESS;
}


ErrorType Game::InitialiseScript()
{
	// Hard code for now
	StaticMap* pMap = StaticMap::GetInstance();

	
	// Add some rectangles
	Rectangle2D r;
	r.PlaceAt(200, 0, 250, 150);
	pMap->AddBlock(r);
	r.PlaceAt(200, 150, 400, 350);
	pMap->AddBlock(r);
	r.PlaceAt(800, 50, 850, 400);
	pMap->AddBlock(r);
	r.PlaceAt(100, 480, 400, 520);
	pMap->AddBlock(r);
	r.PlaceAt(600, 480, 900, 520);
	pMap->AddBlock(r);
	r.PlaceAt(150, 600, 200, 950);
	pMap->AddBlock(r);
	r.PlaceAt(600, 650, 800, 850);
	pMap->AddBlock(r);
	r.PlaceAt(750, 850, 800, 1000);
	pMap->AddBlock(r);

	NodeGraph *pNodeGraph = NodeGraph::GetInstance();

	Rectangle2D temp;
	temp.PlaceAt( Vector2D( -250 , -250 ), Vector2D( 1250, 1250) );

	pNodeGraph->FindNodes( temp );
	pNodeGraph->FindEdges();

	// Add two spawn points
	pMap->AddSpawnPoint(Vector2D(50, 500));
	pMap->AddSpawnPoint(Vector2D(950, 500));

	// Place the domination points
	DynamicObjects* pDynObjects = DynamicObjects::GetInstance();

	ErrorLogger::Writeln( sizeof( DynamicObjects ) );

	pDynObjects->PlaceDominationPoint(Vector2D( 250,50 ));
	pDynObjects->PlaceDominationPoint(Vector2D( 500,500 ));
	pDynObjects->PlaceDominationPoint(Vector2D( 750,950 ));

	pDynObjects->Initialise();

	return SUCCESS;
}