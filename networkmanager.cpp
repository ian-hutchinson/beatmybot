// networkmanager.cpp
// Class for managing network functionality (server version)
// Ian Hutchinson

#include "networkmanager.h"
#include "dynamicobjects.h"

NetworkManager* NetworkManager::instance = NULL;

bool NetworkManager::server_up;
int  NetworkManager::sock;
int  NetworkManager::clients_connected;

const int SENDDATASIZE = sizeof( SendData );

union SendUnion
{
	SendData sd;
	char bytes[SENDDATASIZE];
} sendUnion;

ClientThreadArgs* NetworkManager::cthread_args;

NetworkManager* NetworkManager::Start( unsigned short _port )
{
	if( instance )
		instance->Terminate();

	instance = new NetworkManager( _port );

	server_up = true;

	DWORD threadID;

	if( CreateThread( NULL, 
					  0, 
					  (LPTHREAD_START_ROUTINE) NetworkManager::AcceptTCPConnection,
					  NULL,
					  0,
					  (LPDWORD) &threadID ) == NULL )
	{
		ErrorLogger::Writeln( "CreateThread() failed\n" );
		return NULL;  // Pointless going on if listener thread failed
	}
	ErrorLogger::Writeln( "Listener thread created successfully!" );
	return instance;
}

NetworkManager* NetworkManager::GetInstance()
{
	return instance;
}

NetworkManager::~NetworkManager()
{

}

ErrorType NetworkManager::Terminate()
{
	if( instance )
	{
		delete instance;
		instance = NULL;
		return SUCCESS;
	}
	else
		return FAILURE;
}

NetworkManager::NetworkManager( unsigned short _port )
{
	if( WSAStartup( MAKEWORD( 2, 0 ), &wsa_data ) != NULL )
	{
		ErrorLogger::Writeln( "WSAStartup() failed\n" );
		return;
	}

	if ( CreateTCPServerSocket( _port ) != SUCCESS )
	{
		ErrorLogger::Writeln( "CreateTCPServerSocket failed\n" );
		return;
	}

	ErrorLogger::Writeln( "Startup fine, socket created successfully" );

	clients_connected = 0;
}

ErrorType NetworkManager::CreateTCPServerSocket( unsigned short _port )
{
	ErrorLogger::Writeln( "Creating TCP socket" );
	sockaddr_in serv_addr;
	int result = -1;

	// Create socket for incoming connections
	sock = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP );

	if ( sock < 0 )
	{
		ErrorLogger::Writeln( "socket() failed\n" );
		return FAILURE;
	}

	// Construct local address structure
	memset( &serv_addr, 0, sizeof( serv_addr ) );
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl( INADDR_ANY );
	serv_addr.sin_port = htons( _port );

	// Bind socket to address
	result = bind( sock, (sockaddr *) &serv_addr, sizeof( serv_addr ) );

	if( result < 0 )
	{
		ErrorLogger::Writeln( "bind() failed\n" );
		return FAILURE;
	}

	ErrorLogger::Writeln( sock );

	// Mark the socket so it will listen for incoming connections
	result = listen( sock, MAX_CLIENTS );

	if( result < 0 )
	{
		ErrorLogger::Writeln ( "listen() failed\n" );
		return FAILURE;
	}
    
	return SUCCESS;
}

ErrorType NetworkManager::Update()
{

	// Placeholder
	return SUCCESS;
}

void* NetworkManager::AcceptTCPConnection( void *_args )
{
	ErrorLogger::Writeln( "Entered listener thread" );
	int clnt_sock;
	sockaddr_in clnt_addr;
	unsigned int clnt_len;

	clnt_len = sizeof( clnt_addr );

	while( clients_connected < MAX_CLIENTS )  // This will need to be changed to while server_up
	{
		ErrorLogger::Writeln( "Listening..." );

		// Wait for a client to connect
		clnt_sock = accept( sock, (sockaddr *) &clnt_addr, (int *) &clnt_len );
		// ErrorLogger::Writeln( "Done listening!" );
		
		if( clnt_sock < 0 )
		{
			ErrorLogger::Writeln( "accept() failed\n" );
			return NULL;
		}
		else
		{
			clients_connected++;
			// Client socket connected to client
			ErrorLogger::Write( "Client connected: " );
			ErrorLogger::Writeln( inet_ntoa( clnt_addr.sin_addr ) );

			DWORD threadID;
			// Create memory for client arg
			cthread_args = new ClientThreadArgs;
			cthread_args->clnt_sock = clnt_sock;

			if( CreateThread( NULL, 
							  0, 
							  (LPTHREAD_START_ROUTINE) NetworkManager::HandleClientThread, 
							  cthread_args,
							  0,
							  (LPDWORD) &threadID ) == NULL )
			{
				ErrorLogger::Writeln( "CreateThread() failed\n" );
				return NULL;
			}
		}
	}

	return NULL;
}

void* NetworkManager::HandleClientThread( void *_args )
{
	ErrorLogger::Writeln( "Entered handling client thread" );
	int clnt_sock;
	int sent, size;
	bool dcFlag = false;

	clnt_sock = ( (ClientThreadArgs *)_args )->clnt_sock;
	delete _args;

	bool printed = false;

	// Disable Nagle's algorithm
	int flag = 1;
	setsockopt( clnt_sock, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof( int ) );

	while( server_up )
	{

		/*ErrorLogger::Writeln( sendUnion.sd.teams[0].bots[0].y );*/
		size = sizeof( SendData );
	/*	ErrorLogger::Write( "Size: " );
		ErrorLogger::Writeln( size );*/


		sent = send( clnt_sock, sendUnion.bytes, SENDDATASIZE, 0 );
		
		if( sent != size )
		{
			ErrorLogger::Writeln( "send() failed" );

			if( !dcFlag )
			{
				clients_connected--;
				dcFlag = true;
			}
		}

		//ErrorLogger::Write( "Bytes sent: " );
		ErrorLogger::Writeln( sent );
	}

	closesocket( clnt_sock );

	/*
	if( HandleTCPClient( clnt_sock ) != SUCCESS )
	{
		ErrorLogger::Writeln( "HandleTCPClient failed()\n" );
		return NULL;
	}*/

	return NULL;
}

ErrorType NetworkManager::HandleTCPClient( int _clnt_sock )
{
	/*char test_str[BUFFSIZE];
	int size = sizeof( test_str );

	if( send( _clnt_sock, test_str, size, 0 ) != size )
	{
		ErrorLogger::Writeln( "send() failed\n" );
		return FAILURE;
	}*/

	closesocket( _clnt_sock );

	return SUCCESS;
}

int NetworkManager::GetNumClientConnected()
{
	return clients_connected;
}

void NetworkManager::UpdateData( SendData d )
{
	sendUnion.sd = d;
}