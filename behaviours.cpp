// Behaviours class source file
// Ian Hutchinson
// 09/11/2012

#include "behaviours.h"
#include "bot.h"
#include "NodeGraph.h"
#include "dynamicobjects.h"
#include "ErrorLogger.h"

Behaviours::Behaviours()
{
	m_seekOn = false;
	m_fleeOn = false;
	m_arriveOn = false;
	m_pursueOn = false;
	m_evadeOn = false;
	m_wallAvoidOn = false;
	m_followPathOn = false;

	m_seekMulti = 1.0;
	m_fleeMulti = 1.0;
	m_arriveMulti = 1.0;
	m_pursueMulti = 1.0;
	m_evadeMulti = 1.0;
	m_wallMulti = 18.0;
	m_followMulti = 3.0;
}

Behaviours::~Behaviours()
{
}

void Behaviours::SetOwner( Bot* _owner )
{
	m_owner = _owner;
}

Vector2D Behaviours::Seek( Vector2D _target )
{
	double multiplier = 1.0;

	Vector2D desiredVelocity = ( _target - m_owner->GetLocation() ).unitVector()
		                        * MAXBOTSPEED ;

	return (desiredVelocity - m_owner->GetVelocity()) * multiplier;	
}

Vector2D Behaviours::Flee( Vector2D  _target )
{
	return -Seek( m_target );
}

Vector2D Behaviours::Arrive( Vector2D _target )
{
	Vector2D toTarget = _target - m_owner->GetLocation();

	double distance = toTarget.magnitude();

	if( distance > 0 )
	{
		double decelTweak = 0.97;

		double speed = distance / ( 1 / decelTweak );

		if( speed > MAXBOTSPEED )
			speed = MAXBOTSPEED;

		Vector2D desiredVelocity = toTarget * speed / distance;

		return desiredVelocity - m_owner->GetVelocity();
	}

	return Vector2D( 0, 0 );
}

Vector2D Behaviours::Pursue( Vector2D _target, Vector2D _targetVel )
{
	double distance = ( _target - m_owner->GetLocation() ).magnitude();

	double time = distance / MAXBOTSPEED;

	Vector2D targetPoint = _target + _targetVel * time;


	m_target = targetPoint;

	return Arrive( targetPoint );
}

Vector2D Behaviours::Evade( Vector2D _target, Vector2D _targetVel )
{
	double distance = ( _target - m_owner->GetLocation() ).magnitude();

	double time = distance / MAXBOTSPEED;

	Vector2D targetPoint = _target + _targetVel * time;

	return Flee( targetPoint );
}

Vector2D Behaviours::WallAvoid()
{
	Vector2D normalVector(0,0);
	StaticMap *map = StaticMap::GetInstance();
	//double intensity = 8.0;
	double intensity = 15.0;

	for( int i = 0; i < NUM_SENSORS; i++ )
		normalVector += map->GetNormalToSurface( m_owner->m_sensors[i] );
	
	return normalVector * intensity;

}

Vector2D Behaviours::FollowPath()
{
	if( m_currentPath.size() == 0 )
		return Vector2D( 0, 0 );

	for( int i=0; i < m_currentPath.size(); i++ )
	{
		if( StaticMap::GetInstance()->IsLineOfSight( m_owner->GetLocation(), m_currentPath[i] ) )
		{
			for( int j=i + 1; j < m_currentPath.size(); j++ )
			{
				m_currentPath.pop_back();
			}

			return Arrive( m_currentPath[i] );
		}
	}
	return Arrive( m_currentPath[m_currentPath.size() - 1] );
}


Vector2D Behaviours::AccumulateBehaviours()
{
	m_totalForce = Vector2D( 0, 0 );

	Vector2D force;

	if( m_wallAvoidOn )
	{
		force = WallAvoid() * m_wallMulti;

		if( !AccumulateForce( m_totalForce, force ) )
			return m_totalForce;

	}

	if( m_followPathOn )
	{
		force = FollowPath() * m_followMulti;

		if( !AccumulateForce( m_totalForce, force ) ) 
			return m_totalForce;

	}

	if( m_evadeOn ) 
	{
		force = Evade( m_target, m_targetVel ) * m_evadeMulti;

		if( !AccumulateForce( m_totalForce, force ) )
			return m_totalForce;
	}

	if( m_pursueOn )
	{
		force = Pursue( m_target, m_targetVel ) * m_pursueMulti;

		if( !AccumulateForce( m_totalForce, force ) )
			return m_totalForce;
		
	}
	
	if( m_fleeOn )
	{
		force = Flee( m_target ) * m_fleeMulti;

		if( !AccumulateForce( m_totalForce, force ) )
			return m_totalForce;
	}

	if( m_seekOn )
	{
		force = Seek( m_target ) * m_seekMulti;

		if( !AccumulateForce( m_totalForce, force ) )
			return m_totalForce;
	}
	


	return m_totalForce;
}

void Behaviours::SetPath( Vector2D _target )
{
	NodeGraph *ng = NodeGraph::GetInstance();
	
	ClearPath();

	m_currentPath.push_back( _target );
	
	std::vector<Vector2D> tmp;

	tmp = ng->AStar( ng->GetClosestNode( m_owner->GetLocation() ), ng->GetClosestNode( _target ) ); // First param here would be owner location

	for( int i=0; i < tmp.size(); i++ )
	{
		m_currentPath.push_back( tmp[i] );
	}
}

void Behaviours::AllOff()
{
	m_seekOn = false;
	m_fleeOn = false;
	m_arriveOn = false;
	m_pursueOn = false;
	m_evadeOn = false;
	m_wallAvoidOn = false;
	m_followPathOn = false;
}

void Behaviours::SetSeek( bool _status )
{
	m_seekOn     = _status;
}

void Behaviours::SetFlee( bool _status )
{
	m_fleeOn     = _status;
}

void Behaviours::SetArrive( bool _status )
{
	m_arriveOn = _status;
}

void Behaviours::SetPursue( bool _status )
{
	m_pursueOn = _status;
}

void Behaviours::SetEvade( bool _status )
{
	m_evadeOn = _status;
}

void Behaviours::SetWallAvoid( bool _status )
{
	m_wallAvoidOn = _status;
}

void Behaviours::SetFollowPath( bool _status )
{
	m_followPathOn = _status;
}

void Behaviours::SetTarget( Vector2D _target )
{
	m_target = _target;
}

void Behaviours::SetTargetVelocity( Vector2D _vel )
{
	m_targetVel = _vel;
}


bool Behaviours::AccumulateForce( Vector2D &_runningTotal, Vector2D _forceToAdd )
{
	// Calculate how much force is used so far
	double magSoFar = _runningTotal.magnitude();

	// Calculate how much remaining force
	double magRemaining = MAXIMUMFORCE - magSoFar;

	// Return false if there's no force left
	if( magRemaining <= 0.0 )
		return false;

	// Calculate the maginute of force to add
	double magToAdd = _forceToAdd.magnitude();

	// If the magniute to add plus the running total does not exceed the
	// maximum force, add together.  Otherwise, add as much as possible.

	if( magToAdd < magRemaining )
		_runningTotal += _forceToAdd;
	else
		_runningTotal += ( _forceToAdd.unitVector() * magRemaining );

	return true;
}

void Behaviours::DrawPath() 
{
	if( m_currentPath.size() > 1 )
	{
		for( int i=0; i < m_currentPath.size() - 1; i++ )
		{
			Renderer::GetInstance()->DrawLine( m_currentPath[i], m_currentPath[i+1], 6 );
		}
	}
}

void Behaviours::ClearPath()
{
	m_currentPath.clear();
}

void Behaviours::WriteTotalForce() const
{
	Renderer::GetInstance()->DrawNumberAt( Vector2D( 500, 700 ), m_totalForce.magnitude() );
}

Vector2D Behaviours::GetTarget() const
{
	return m_target;
}