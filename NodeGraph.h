// NodeGraph.h

#include "Node.h"
#include "Shapes.h"
#include "staticmap.h"
#include <vector>
#include "Renderer.h"
#include "errorlogger.h"
#include "ErrorType.h"

const int SUBDIV_RECTS = 4;

class NodeGraph
{
private:
	std::vector<Node> m_nodeList;
	bool m_drawNodes;
	bool m_drawNodeNums;
	static int m_idxCounter;
	static NodeGraph* instance;
	NodeGraph();
	~NodeGraph();
public:
	static NodeGraph* GetInstance();
	void Release();
	void AddNode( Node _newNode );
	Node GetNode( int _idx );
	int GetNumNodes();
	int GetClosestNode( Vector2D _position );
	void FindNodes( Rectangle2D _rect );
	void FindEdges();
	void DrawAllNodes();
	void DrawAllEdges();
	bool DrawModeEnabled();
	bool OpenListEntriesExist();
	void Reset();
	void Explore( int _node, int _goal );
	int FindLowestF();
	double Heuristic( int _from, int _to );
	void GetPath( int _current, std::vector<int> &_nodes );
	std::vector<Vector2D> AStar( int _start, int _goal );
	void DrawPath( std::vector<Vector2D> _path );
};