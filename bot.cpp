#include "bot.h"
#include "staticmap.h"
#include "dynamicobjects.h"
#include "renderer.h"
#include <cmath>
#include "NodeGraph.h"
#include "substates.h"
#include "FuzzySubstates.h"

void Bot::Update(double frametime)
{
	ProcessAI();
	// Check for respawn
	if(this->m_dTimeToRespawn>0)
	{
		m_dTimeToRespawn-=frametime;
		this->m_Velocity.set(0,0);
		if(m_dTimeToRespawn<=0)
		{
			PlaceAt(StaticMap::GetInstance()->GetSpawnPoint(m_iOwnTeamNumber));
		}
	}
	else
	{
		// Get AI instructions ************************************************


		// Apply physics ***************************************************************
		
		// Disable movement for aiming bot
		if(this->m_bAiming == true)
		{
			Bot& targetBot = DynamicObjects::GetInstance()->
											GetBot(m_iAimingAtTeam, m_iAimingAtBot);

			if( m_Velocity.magnitude() > 0.1 )
				m_Acceleration = -m_Velocity.unitVector()*MAXIMUMSPEED;
			else
				m_Acceleration.set( 0, 0 );

			m_dDirection = (targetBot.m_Position-m_Position).angle();
		}

		// Clamp acceleration to maximum
		if(m_Acceleration.magnitude()>MAXIMUMACCELERATION)
		{
			m_Acceleration=m_Acceleration.unitVector()*MAXIMUMACCELERATION;
		}

		// Accelerate
		m_Velocity+=m_Acceleration*frametime;

		// Clamp speed to maximum
		if(m_Velocity.magnitude()>MAXIMUMSPEED)
		{
			m_Velocity=m_Velocity.unitVector()*MAXIMUMSPEED;
		}

		if(m_Velocity.magnitude()>0.1 && !m_bAiming)
		{
			m_dDirection = m_Velocity.angle();
		}

		// Move
		m_Position+=m_Velocity*frametime;

		// Check for collision
		Circle2D pos(m_Position, 8);

		if(StaticMap::GetInstance()->IsInsideBlock(pos))
		{
			// Collided. Move back to where you were.
			m_Position-=m_Velocity*frametime;
			m_Velocity.set(0,0);
		}

		// Handle shooting ***********************************************

		if(	m_dTimeToCoolDown>0)
			m_dTimeToCoolDown-=frametime;

		if(m_bAiming == true)
		{
			// If target is not valid
			if(m_iAimingAtTeam<0 ||m_iAimingAtTeam>=NUMTEAMS 
				||m_iAimingAtBot<0 ||m_iAimingAtBot>=NUMBOTSPERTEAM  )
			{
				m_bAiming = false;
				m_dAccuracy =0;
			}
			// else if target is dead
			else if(!DynamicObjects::GetInstance()->GetBot(m_iAimingAtTeam, m_iAimingAtBot).IsAlive())
			{
				m_bAiming = false;
				m_dAccuracy =0;
			}
			else if(m_dTimeToCoolDown>0)
			{
				// Can't shoot. Waiting to cool down
				Bot& targetBot = DynamicObjects::GetInstance()->
												GetBot(m_iAimingAtTeam, m_iAimingAtBot);
				if(m_Velocity.magnitude()<=0.1)
				{
					m_dDirection = (targetBot.m_Position-m_Position).angle();
				}
			}
			else		// Valid target
			{
				Bot& targetBot = DynamicObjects::GetInstance()->
												GetBot(m_iAimingAtTeam, m_iAimingAtBot);
				
				if(m_Velocity.magnitude()<=0.1)
				{
					m_dDirection = (targetBot.m_Position-m_Position).angle();
				}

				// Do we have line of sight?
				if(StaticMap::GetInstance()->IsLineOfSight(this->m_Position, targetBot.m_Position))
				{
					double range = (targetBot.m_Position-m_Position).magnitude();
					
					if(range<1.0)
						range = 1.0;		// Probably aiming at yourself
											// Otherwise suicide causes a divide by zero error.
											// That's the real reason why the church is against it.

					double peakAccuracy = sqrt(ACCURATERANGE / range);
					
					m_dAccuracy += (peakAccuracy - m_dAccuracy)*frametime;

					if(m_bFiring==true && m_dTimeToCoolDown<=0)
					{
						m_bFiring = false;

						// Take the shot
						m_dTimeToCoolDown = TIMEBETWEENSHOTS;
						int damage=0;
						if(m_dAccuracy>0.5)
						{
							int x=0;
						}

						while(damage<100 && (rand()%1000) < int(m_dAccuracy*1000))
						{
							damage+=20;
						}

						targetBot.TakeDamage(damage);
						m_damage = damage;
						m_dAccuracy/=2.0;

						Vector2D tgt = targetBot.m_Position;
						if(damage==0)	// you missed
						{
							// Make it look like a miss
							tgt+=Vector2D(rand()%30-15, rand()%30-15);
						}
						shotTarget = tgt;
						sendShot = true;
						Renderer::GetInstance()->AddShot(m_Position, tgt);
						Renderer::GetInstance()->AddBloodSpray(targetBot.m_Position, targetBot.m_Position-m_Position, damage/5);
					}

				}
				else		// No line of sight
				{
					m_bAiming = false;
					m_dAccuracy =0;		
				}

			}			// End valid target
		}
		else			// Not aiming
		{
			m_dAccuracy =0;
		}
	}

}



Vector2D Bot::GetLocation()
{
	return m_Position;
}

double Bot::GetDirection()
{
	return this->m_dDirection;
}

Vector2D Bot::GetVelocity()
{
	return m_Velocity;
}

void Bot::SetOwnNumbers(int teamNo, int botNo)
{
	m_iOwnTeamNumber = teamNo;
	m_iOwnBotNumber = botNo;
}

void Bot::PlaceAt(Vector2D position)
{
	m_Position = position;	// Current world coordinates
	m_Velocity.set(0,0);	// Current velocity
	m_dDirection=0;			// Direction. Mainly useful when stationary
	m_bAiming=false;		// If true, bot is aiming and cannot move
	m_dTimeToCoolDown=0;	// Countdown until the time the bot can shoot again
	m_dTimeToRespawn=0;		// Countdown until the bot can respawn. If zero or below, bot is alive
	m_Acceleration.set(0,0);
	m_bFiring=false;
	m_dAccuracy =0;
	m_iHealth=100;
}

bool Bot::IsAlive()
{
	if(m_dTimeToRespawn<=0)
		return true;
	else
		return false;
}

Bot::Bot()
{
	// I suggest you do nothing here.
	// Remember that the rest of the world may not have been created yet.
	PlaceAt(Vector2D(0,0));		// Places bot at a default location
	m_dTimeToRespawn=1;
	m_drawSensors = false;
	behaviours.SetOwner( this );
	m_stateMachine = new StateMachine<Bot>( this );
	m_initialSpawn = true;
}

Bot::~Bot()
{
	if( m_stateMachine )
	{
		delete m_stateMachine;
		m_stateMachine = NULL;
	}
}


int Bot::GetHealth()
{
	return m_iHealth;
}

double Bot::GetAccuracy()
{	
	if(m_bAiming== true)
		return m_dAccuracy;
	else
		return 0;
}

void Bot::SetTarget(int targetTeamNo, int targetBotNo)
{

	if(m_iAimingAtTeam!=targetTeamNo || m_iAimingAtBot!=targetBotNo)
	{

		m_iAimingAtTeam = targetTeamNo;

		m_iAimingAtBot = targetBotNo;

		m_dAccuracy=0;
	}

	m_bAiming = true;
}

// Stops the bot from aiming, so it can move again
void Bot::StopAiming()
{
	m_bAiming = false;
}

// Call this to set the bot to shoot, if it can.
// Once a shot it taken, the bot will not shoot again unless told to do so.
void Bot::Shoot()
{
	m_bFiring = true;
}

// Returns the number of the team of the bot being aimed at.
// Returns a negative number if no bot is being aimed at.
int Bot::GetTargetTeam()
{
	if(m_bAiming== true)
		return m_iAimingAtTeam;
	else
		return -1;
}

// Returns the number of the bot being aimed at.
// Returns a negative number if no bot is being aimed at.
int Bot::GetTargetBot()
{
	if(m_bAiming== true)
		return m_iAimingAtBot;
	else
		return -1;
}

void Bot::TakeDamage(int amount)
{
	m_iHealth-=amount;

	if(m_iHealth<=0 && m_dTimeToRespawn<=0)
	{
		m_dTimeToRespawn = RESPAWNTIME;
	}

	// Being shot at puts you off your aim. 
	// Even more so if you are hit

	if(amount>0)
		m_dAccuracy=0;
	else
		m_dAccuracy/=2;
}

// ****************************************************************************

// This is your function. Use it to set up any states at the beginning of the game
// and analyse the map.
// Remember that bots have not spawned yet, so will not be in their
// starting positions.
// Eventually, this will contain very little code - it just sets a starting state
// and calls methods to analyse the map
void Bot::StartAI()
{
	if( m_iOwnTeamNumber == 0 )
		m_stateMachine->SetCurrentState( FuzzySpawn::GetInstance() );
	else
		m_stateMachine->SetCurrentState( Spawn::GetInstance() );
}

// This is your function. Use it to set the orders for the bot.
// Will be called once each frame from Update
// Remember this will be called even if the bot is currently dead
// Eventually, this will contain very little code - it just runs the state   
void Bot::ProcessAI()
{
	DrawBotNumber();
	UpdateSensors();
	m_stateMachine->Update();
	m_Acceleration = behaviours.AccumulateBehaviours();
	// behaviours.DrawPath();
}

void Bot::UpdateSensors()
{
	Vector2D temp;
	Renderer *r = Renderer::GetInstance();

	temp.setBearing( m_dDirection, SENSOR_LEN );
	m_sensors[0] = m_Position + temp;

	temp.setBearing( m_dDirection + 0.78, SENSOR_LEN );
	m_sensors[1] = m_Position + temp;

	temp.setBearing( m_dDirection - 0.78, SENSOR_LEN );
	m_sensors[2] = m_Position + temp;

	temp.setBearing( m_dDirection + 1.57, SENSOR_LEN );
	m_sensors[3] = m_Position + temp;

	temp.setBearing( m_dDirection - 1.57, SENSOR_LEN );
	m_sensors[4] = m_Position + temp;
	
	
	if( m_drawSensors )
	{
		for( int i=0; i < NUM_SENSORS; i++ )
			r->DrawLine( m_Position, m_sensors[i], RGB( 0, 255, 0 ) );
	} 
	
	//if( m_iHealth > 0 )
	//	r->DrawLine( m_Position, m_Position + m_Acceleration, 5 );
}

void Bot::SetPath( Vector2D _target )
{
	behaviours.SetPath( _target );
}

Bot* Bot::FindMostDangerousEnemy()
{
	DynamicObjects *d = DynamicObjects::GetInstance();

	Bot *mostDangerous = &( d->GetBot( 1 - m_iOwnTeamNumber, 0 ) );
	Bot *current;

	for( int i=1; i < NUMBOTSPERTEAM; i++ )
	{
		current = &(d->GetBot( 1 - m_iOwnTeamNumber, i ) );

		if( ( current->GetLocation() - m_Position ).magnitude() <
			( mostDangerous->GetLocation() - m_Position ).magnitude() )
		{
			mostDangerous = current;
		}
	}

	return mostDangerous;
}

int Bot::GetTeamNumber() const
{
	return m_iOwnTeamNumber;
}

int Bot::GetBotNumber() const
{
	return m_iOwnBotNumber;
}

void Bot::DrawBotNumber() const
{
	Renderer *r = Renderer::GetInstance();
	std::string str;
	
	//if( m_iHealth > 0 )
	//	Renderer::GetInstance()->DrawNumberAt( Renderer::GetInstance()->ScreenPosition( m_Position ), m_iOwnBotNumber );

	if( m_iHealth > 0 )
	{
		if( m_stateMachine->IsInState( CaptureDomPoint::GetInstance() ) ||
			m_stateMachine->IsInState( FuzzyCaptureDomPoint::GetInstance() ) )
		{
			str = "CDP";
			Vector2D tmp = m_Position;
			tmp.YValue += 20;
			r->DrawNumberAt( r->ScreenPosition( tmp ), m_currentDPTarget );
		}
		else if( m_stateMachine->IsInState( Attack::GetInstance() ) ||
		         m_stateMachine->IsInState( FuzzyAttack::GetInstance() ) )
		{
			str = "A";
			Vector2D tmp = m_Position;
			tmp.YValue += 20;
			r->DrawNumberAt( r->ScreenPosition( tmp ), this->m_nearestEnemy );
		}
		else if( m_stateMachine->IsInState( Flee::GetInstance() ) ||
			     m_stateMachine->IsInState( FuzzyFlee::GetInstance() ) )
		{
			str = "F";
		}
		else if( m_stateMachine->IsInState( Defend::GetInstance() ) ||
			     m_stateMachine->IsInState( FuzzyDefend::GetInstance() ) )
		{
			str = "D";
			Vector2D tmp = m_Position;
			tmp.YValue += 20;
			r->DrawNumberAt( r->ScreenPosition( tmp ), this->m_currentDPTarget );
		}

		else if( m_stateMachine->IsInState( Spawn::GetInstance() ) ||
			     m_stateMachine->IsInState( FuzzySpawn::GetInstance() ) )
		{
			str = "S";
		}

		r->DrawTextAt( r->ScreenPosition( m_Position ), (char*) str.c_str() );
	}
}

StateMachine<Bot>* Bot::GetFSM() const
{
	return m_stateMachine;
}

void Bot::SetCurrentDPTarget( int _dp )
{
	m_currentDPTarget = _dp;
}

int Bot::GetCurrentDPTarget() const
{
	return m_currentDPTarget;
}

void Bot::SetNearestEnemy( int _enemy )
{
	m_nearestEnemy = _enemy;
}

int Bot::GetNearestEnemy() const
{
	return m_nearestEnemy;
}

double Bot::GetRespawnTime() const
{
	return this->m_dTimeToRespawn;
}

bool Bot::InitialSpawn() const
{
	return m_initialSpawn;
}

void Bot::DeactivateInitialSpawn()
{
	m_initialSpawn = false;
}

bool Bot::SendShot() const
{
	return sendShot;
}

void Bot::SetSendShot( bool b )
{
	sendShot = b;
}

Vector2D Bot::GetShotTarget() const
{
	return shotTarget;
}

int Bot::GetDamageDone() const
{
	return m_damage;
}

double Bot::GetCooldown() const
{
	return m_dTimeToCoolDown;
}