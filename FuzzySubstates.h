#ifndef __FUZZY_SUBSTATES_H__
#define __FUZZY_SUBSTATES_H__

#include "state.h"
#include "bot.h"
#include "dynamicobjects.h"
#include "staticmap.h"
#include "rules.h"
#include "FuzzyLogic.h"

class FuzzyFlee: public State<Bot>
{
private:
	FuzzyFlee();
	~FuzzyFlee();
	FuzzyFlee( const Flee &other );
	static FuzzyFlee *m_instance;
	Bot *m_enemy;
public:
	static FuzzyFlee* GetInstance();
	static void Release();
	bool Escaped( Bot *_sender );
	bool NobodyAimingAtMe( Bot *_sender );
	void Enter( Bot *_sender );
	void Exit( Bot *_sender );
	void Execute( Bot *_sender );
};

class FuzzyCaptureDomPoint: public State<Bot>
{
private:
	FuzzyCaptureDomPoint();
	~FuzzyCaptureDomPoint();
	FuzzyCaptureDomPoint( const CaptureDomPoint &other );
	static FuzzyCaptureDomPoint *m_instance;
public:
	static FuzzyCaptureDomPoint* GetInstance();
	static void Release();
	int GetDomPoint( Bot *_sender );
	int NumTeammatesHeadingToDP( Bot *_sender, int _dp );
	int ScoreDifference( Bot *_sender );
	int EnemiesNearDP( Bot *_sender, int _dp );
	bool DPIsUnmanned( Bot *_sender, int _dp );
	bool EnemyNearby( Bot *_sender );
	bool TeammateNearby( Bot *_sender );
	void Enter( Bot *_sender );
	void Exit( Bot *_sender );
	void Execute( Bot *_sender );
};

class FuzzyAttack: public State<Bot>
{
private:
	FuzzyAttack();
	~FuzzyAttack();
	FuzzyAttack( const Attack &other );
	static FuzzyAttack *m_instance;
public:
	static FuzzyAttack* GetInstance();
	static void Release();
	bool EnemyNearby( Bot *_sender );
	bool CurrentEnemyOutOfRange( Bot *_sender );
	bool TeamAttackingSameBot( Bot *_sender );
	bool TeamNearby( Bot *_sender );
	void Enter( Bot *_sender );
	void Execute( Bot *_sender );
	void Exit( Bot *_sender );
};

class FuzzyDefend: public State<Bot>
{
private:
	FuzzyDefend();
	~FuzzyDefend();
	FuzzyDefend( const Defend &other );
	static FuzzyDefend *m_instance;
public:
	static FuzzyDefend* GetInstance();
	static void Release();
	int TeammatesNearDP( Bot *_sender, int _dp );
	int TeammatesHeadingToDP( Bot *_sender, int _dp );
	int GetNearestDPToDefend( Bot *_sender );
	bool EnemyNearby( Bot *_sender );
	void Enter( Bot *_sender );
	void Execute( Bot *_sender );
	void Exit( Bot *_sender );
};

//class Chase: public State<Bot>
//{
//private:
//	Chase();
//	~Chase();
//	Chase( const Chase &other );
//	static Chase *m_instance;
//public:
//	static Chase* GetInstance();
//	static void Release();
//	void Enter( Bot *_sender );
//	void Execute( Bot *_sender );
//	void Exit( Bot *_sender );
//};

class FuzzySpawn: public State<Bot>
{
private:
	FuzzySpawn();
	~FuzzySpawn();
	FuzzySpawn (const Spawn &other );
	static FuzzySpawn *m_instance;
public:
	static FuzzySpawn* GetInstance();
	static void Release();
	int ScoreDifference( Bot *_sender );
	void Enter( Bot *_sender );
	void Execute( Bot *_sender );
	void Exit( Bot *_sender );
};

#endif