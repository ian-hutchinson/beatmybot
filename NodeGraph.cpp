// NodeGraph.cpp

#include "NodeGraph.h"

int NodeGraph::m_idxCounter;
NodeGraph* NodeGraph::instance = NULL;

void NodeGraph::AddNode( Node _newNode )
{
	m_nodeList.push_back( _newNode );
}

Node NodeGraph::GetNode( int _idx )
{
	return m_nodeList[_idx];
}

int NodeGraph::GetNumNodes()
{
	return m_nodeList.size();
}

int NodeGraph::GetClosestNode( Vector2D _position )
{
	int ret = 0;

	for( int i=0; i < m_nodeList.size(); i++ )
	{
		if( ( _position - m_nodeList[i].GetLocation() ).magnitude() <
			( _position - m_nodeList[ret].GetLocation() ).magnitude() )
		{
			ret = i;
		}
	}

	return ret;
}

NodeGraph::NodeGraph()
{
	m_drawNodes = false;
	m_drawNodeNums = false;
	m_idxCounter = 0;
}

NodeGraph::~NodeGraph()
{
}

void NodeGraph::FindNodes( Rectangle2D _rect )
{
	if( _rect.GetTopRight().XValue - _rect.GetTopLeft().XValue >= 25 )
	{
		if( StaticMap::GetInstance()->IsInsideBlock( _rect ) )
		{
			// Inside of block, time to subdivide

			Rectangle2D rects[SUBDIV_RECTS];

			// First is easy: TL is TL of original rect, BR is centre point 
			rects[0].PlaceAt( _rect.GetTopLeft(), _rect.GetCentre() );

			// Second is a little more tricky
			Vector2D tempTL, tempBR;

			tempTL.set( _rect.GetCentre().XValue, _rect.GetTopLeft().YValue );
			tempBR.set( _rect.GetBottomRight().XValue, _rect.GetCentre().YValue );

			rects[1].PlaceAt( tempTL, tempBR );

			// As is the third
			tempTL.set( _rect.GetTopLeft().XValue, _rect.GetCentre().YValue );
			tempBR.set( _rect.GetCentre().XValue, _rect.GetBottomRight().YValue );
			rects[2].PlaceAt( tempTL, tempBR );

			// The fourth is easy: TL is centre point, BR is BR of original rect
			rects[3].PlaceAt( _rect.GetCentre(), _rect.GetBottomRight() );

			for( int i=0; i < SUBDIV_RECTS; i++ )
			{
				FindNodes( rects[i] );
			}
		}
		else
		{
			// Not intersecting a block so add node at centre of rect
			Node tempNode;
			tempNode.SetLocation( _rect.GetCentre() );
			tempNode.SetIndex( m_idxCounter );

			m_nodeList.push_back( tempNode );
			m_idxCounter++;
		}
	}
}

void NodeGraph::DrawAllNodes()
{

	std::vector<Node>::iterator it;

	it = m_nodeList.begin();
	while( it != m_nodeList.end() )
	{
		if( m_drawNodes )
			Renderer::GetInstance()->DrawDot( it->GetLocation(), 3 ) ;

		if( m_drawNodeNums )
			Renderer::GetInstance()->DrawNumberAt( Renderer::GetInstance()->ScreenPosition( it->GetLocation() ), it->GetIndex() );
		it++;
	}
}

void NodeGraph::FindEdges()
{
	std::vector<Node>::iterator i;
	std::vector<Node>::iterator j;

	i = m_nodeList.begin();

	while ( i != m_nodeList.end() )
	{
		j = m_nodeList.begin();

		while( j != m_nodeList.end() )
		{
			if( StaticMap::GetInstance()->IsLineOfSight( i->GetLocation(), j->GetLocation() ) )
			{
				i->AddEdge( Edge( i->GetIndex(), j->GetIndex(), (j->GetLocation() - i->GetLocation()).magnitude() ) );
			}
			j++;
		}

		i++;
	}
}

void NodeGraph::DrawAllEdges()
{
	std::vector<Node>::iterator it;

	it = m_nodeList.begin();

	while( it != m_nodeList.end() )
	{
		for( int i = 0; i < it->GetListEntries(); i++ )
		{
			if( it->GetCostAtIndex( i ) )
			{
				Renderer::GetInstance()->DrawLine( m_nodeList[ it->GetFromIndex( i ) ].GetLocation(), 
												   m_nodeList[ it->GetToIndex( i ) ].GetLocation(),
												   5 );
			}
		}

		it++;
	}

}

void NodeGraph::Reset()
{
	std::vector<Node>::iterator it;

	it = m_nodeList.begin();

	while( it != m_nodeList.end() )
	{
		it->Reset();
		it++;
	}
}

bool NodeGraph::DrawModeEnabled()
{
	if( m_drawNodes || m_drawNodeNums )
		return true;
	
	return false;
}

void NodeGraph::Explore( int _node, int _goal )
{
	m_nodeList[_node].SetStatus( CLOSED );


	int neighbourIdx;

	for( int i = 0; i < m_nodeList[_node].GetListEntries(); i++ )
	{
		neighbourIdx = m_nodeList[_node].GetToIndex( i );

		if( m_nodeList[ neighbourIdx ].GetStatus() == UNEXPLORED )
		{
			// Set unexplored neighbouring nodes to open and set their G score
			m_nodeList[ neighbourIdx ].SetStatus( OPEN );
			m_nodeList[ neighbourIdx ].SetGScore( m_nodeList[_node].GetGScore() + m_nodeList[_node].GetCostAtIndex( i ) );
			m_nodeList[ neighbourIdx ].SetHScore( Heuristic( neighbourIdx, _goal ) );
			m_nodeList[ neighbourIdx ].CalculateFScore();
			m_nodeList[ neighbourIdx ].SetPreviousNode( _node );
		}
		else if ( m_nodeList[ neighbourIdx ].GetStatus() == OPEN )
		{
			// Check open nodes to see if the new G value is lower
			if( m_nodeList[ neighbourIdx ].GetGScore() >
				m_nodeList[ _node ].GetGScore() + m_nodeList[_node].GetCostAtIndex( i ) )
			{
				m_nodeList[ neighbourIdx ].SetGScore( m_nodeList[_node].GetGScore() + m_nodeList[_node].GetCostAtIndex( i ) );
				m_nodeList[ neighbourIdx ].CalculateFScore();
				m_nodeList[ neighbourIdx ].SetPreviousNode( _node );
			}
		}
	}
}

int NodeGraph::FindLowestF()
{
	std::vector<Node>::iterator it;
	int tempIndex = -1;

	it = m_nodeList.begin();

	// Find the first open node
	while( it->GetStatus() != OPEN )
	{
		it++;
	}

	// Use that as the first node for comparison
	tempIndex = it->GetIndex();

	it = m_nodeList.begin();

	while ( it != m_nodeList.end() )
	{
		if( it->GetFScore() < m_nodeList[tempIndex].GetFScore() 
			&& it->GetStatus() == OPEN )
		{
			tempIndex = it->GetIndex();
		}

		it++;
	}

	return tempIndex;
}

double NodeGraph::Heuristic( int _from, int _to )
{
	return ( m_nodeList[_to].GetLocation() - m_nodeList[_from].GetLocation() ).magnitude();
}

void NodeGraph::GetPath( int _current, std::vector<int> &_nodes )
{

	if( m_nodeList[_current].GetPreviousNode() == -1 )
	{
		_nodes.push_back( _current );
	}
	else
	{
		_nodes.push_back( _current );
		GetPath( m_nodeList[_current].GetPreviousNode(), _nodes );
	}
}

bool NodeGraph::OpenListEntriesExist()
{
	for( int i = 0; i < m_nodeList.size(); i++ )
	{
		if( m_nodeList[i].GetStatus() == OPEN )
			return true;
	}

	return false;
}

std::vector<Vector2D> NodeGraph::AStar( int _start, int _goal )
{
	std::vector<int> path;
	std::vector<Vector2D> locPath;

	Reset();

	m_nodeList[_start].SetHScore( Heuristic( _start, _goal ) );
	m_nodeList[_start].CalculateFScore();

	m_nodeList[_start].SetStatus( OPEN );


	while( OpenListEntriesExist() )
	{
		int current = FindLowestF();

		if( current == _goal )
		{
			GetPath( current, path );

			for( int i=0; i < path.size(); i++ )
			{
				locPath.push_back( m_nodeList[path[i]].GetLocation() );
			}
			return locPath;
		}

		Explore( current, _goal );
	}

	return locPath;	
}

void NodeGraph::DrawPath( std::vector<Vector2D> _path )
{
	if( _path.size() > 1 )
	{
		for( int i = 0; i < _path.size() - 1; i++ )
			Renderer::GetInstance()->DrawLine( _path[i], _path[i+1], 5 );
	}
}

NodeGraph* NodeGraph::GetInstance()
{
	if( instance == NULL )
		instance = new NodeGraph();

	return instance;
}

void NodeGraph::Release()
{
	if( instance )
	{
		delete instance;
		instance = NULL;
	}
}