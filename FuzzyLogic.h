// Fuzzy Logic Class

// Health Boundaries
const double H_LOW_LOWER    = 0.0;
const double H_LOW_UPPER    = 30.0;
const double H_MED_LOWER    = 20.0;
const double H_MED_PLAT_L   = 30.0;
const double H_MED_PLAT_R   = 60.0;
const double H_MED_UPPER    = 70.0;
const double H_HIGH_LOWER   = 60.0;
const double H_HIGH_UPPER   = 80.0;

// Range Boundaries
const double R_CLOSE_LOWER  = 0.0;
const double R_CLOSE_UPPER  = 200.0;
const double R_MED_LOWER    = 150.0;
const double R_MED_PLAT_L   = 300.0;
const double R_MED_PLAT_R   = 400.0;
const double R_MED_UPPER    = 500.0;
const double R_FAR_LOWER    = 450.0;
const double R_FAR_UPPER    = 600.0;


// Score Boundaries
const double S_SMALL_LOWER  = 0.0;
const double S_SMALL_UPPER  = 15.0;
const double S_LARGE_LOWER  = 10.0;
const double S_LARGE_UPPER  = 20.0;

// Accuracy Boundaries
const double A_LOW_LOWER    = 0.0;
const double A_LOW_UPPER    = 0.3;
const double A_MED_LOWER    = 0.2;
const double A_MED_PLAT_L   = 0.4;
const double A_MED_PLAT_R   = 0.5;
const double A_MED_UPPER    = 0.7;
const double A_HIGH_LOWER   = 0.65;
const double A_HIGH_UPPER   = 0.9;


class FuzzyBase
{
public:
	static double Downslope( double x, double l, double r );
	static double Upslope( double x, double l, double r );
	static double Not( double a );
	static double And( double lhs, double rhs );
	static double Or( double lhs, double rhs );
	static double Extend( double a ); // For testing
};

class FuzzyHealth: public FuzzyBase
{
public:
	static double GetLowMembership( double x );
	static double GetMediumMembership( double x );
	static double GetHighMembership( double x );
};

class FuzzyRange: public FuzzyBase
{
public:
	static double GetCloseMembership( double x );
	static double GetMediumMembership( double x );
	static double GetFarMembership( double x );
};

class FuzzyScore: public FuzzyBase
{
public:
	static double GetSmallMembership( double x );
	static double GetLargeMembership( double x );
};

class FuzzyAccuracy: public FuzzyBase
{
public:
	static double GetLowMembership( double x );
	static double GetMediumMembership( double x );
	static double GetHighMembership( double x );
};