#include "substates.h"
#include "ErrorLogger.h"
#include "time.h"
#include "gametimer.h"

//struct DomAssessor
//{
//	int index;
//	double score;
//};

const double ATTACK_RANGE = 425.0;

// Flee

Flee* Flee::m_instance = NULL;

Flee::Flee()
{

}

Flee::~Flee()
{

}

Flee* Flee::GetInstance()
{
	if( m_instance == NULL )
		m_instance = new Flee();

	return m_instance;
}

void Flee::Release()
{
	if( m_instance )
	{
		delete m_instance;
		m_instance = NULL;
	}
}

void Flee::Enter( Bot *_sender )
{
	// Behaviour toggles
	_sender->behaviours.AllOff();
	_sender->behaviours.SetFlee( true );
	_sender->behaviours.SetWallAvoid( true );
}

void Flee::Execute( Bot *_sender )
{
	int enemyTeamNo = 1 - _sender->GetTeamNumber();
	DynamicObjects *d = DynamicObjects::GetInstance();

	if( Escaped( _sender ) )
	{
		if( d->GetNumCapturePoints( _sender->GetTeamNumber() ) > 1 &&
			( d->GetScore( _sender->GetTeamNumber() ) - d->GetScore( 1 - _sender->GetTeamNumber() ) ) > -15 )
		{
			_sender->GetFSM()->ChangeState( Defend::GetInstance() );
		}
		else
		{
			_sender->GetFSM()->ChangeState( CaptureDomPoint::GetInstance() );
		}
	}
}

void Flee::Exit( Bot *_sender )
{
	_sender->behaviours.AllOff();
}

bool Flee::Escaped( Bot *_sender )
{
	int enemyTeamNo = 1 - _sender->GetTeamNumber();
	DynamicObjects *d = DynamicObjects::GetInstance();


	if( NobodyAimingAtMe( _sender ) )
	{
		// Nobody is aiming any more, probably be ok
		return true;
	}

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		if( ( (_sender->GetLocation() - d->GetBot( enemyTeamNo, i ).GetLocation()).magnitude() < 450.0 &&
			   d->GetBot( enemyTeamNo, i ).IsAlive() ) )
			return false;
	}

	return true;
}

bool Flee::NobodyAimingAtMe( Bot *_sender )
{
	int enemyTeamNo = 1 - _sender->GetTeamNumber();
	DynamicObjects *d = DynamicObjects::GetInstance();

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		if( d->GetBot( enemyTeamNo, i ).GetTargetBot() == _sender->GetBotNumber() )
		{
			return false;
		}
	}

	return true;
}

// Capture Domination Point

CaptureDomPoint* CaptureDomPoint::m_instance = NULL;

CaptureDomPoint::CaptureDomPoint()
{

}

CaptureDomPoint::~CaptureDomPoint()
{

}

CaptureDomPoint* CaptureDomPoint::GetInstance()
{	
	if( m_instance == NULL )
	{
		m_instance = new CaptureDomPoint();
		srand( time( NULL ) );
	}

	return m_instance;
}

void CaptureDomPoint::Release()
{
	if( m_instance )
	{
		delete m_instance;
		m_instance = NULL;
	}
}

void CaptureDomPoint::Enter( Bot *_sender )
{
	int target = -1;

	// The very start of the game becomes too predicatable
	// if we use the dom point logic.
	// Go random. The points are reassessed in execute anyway.
	if( _sender->InitialSpawn() )
	{
		target = rand()%3;
		_sender->DeactivateInitialSpawn();
	}
	else
	{
		target = GetDomPoint( _sender );
	}

	// If we get -1, there's no DPs to capture.
	// Defend instead
	if( target == -1 )
	{
		target = rand()%3;
	}

	// At this point, we have a target
	// Let the bot know and set a path
	_sender->SetCurrentDPTarget( target );
	_sender->behaviours.SetTarget( DynamicObjects::GetInstance()->GetDominationPoint( _sender->GetCurrentDPTarget() ).m_Location );
	_sender->behaviours.ClearPath();
	_sender->behaviours.SetPath( DynamicObjects::GetInstance()->GetDominationPoint( _sender->GetCurrentDPTarget() ).m_Location );
		
	_sender->behaviours.AllOff();

	// Behaviours set to true
	_sender->behaviours.SetFollowPath( true );
	_sender->behaviours.SetWallAvoid( true );
}

void CaptureDomPoint::Execute( Bot *_sender )
{
	DynamicObjects *d = DynamicObjects::GetInstance();
	GameTimer cpu;

	cpu.mark();
	
	if( EnemyNearby( _sender ) )
	{
		if( _sender->GetHealth() >= 50 ||
			( _sender->GetHealth() < 50 &&
			  TeammateNearby( _sender ) ) )
		{
			_sender->GetFSM()->ChangeState( Attack::GetInstance() );
		}
		else
		{
			_sender->GetFSM()->ChangeState( Flee::GetInstance() );
		}
	}
	else if( d->GetDominationPoint( _sender->GetCurrentDPTarget() ).m_OwnerTeamNumber == _sender->GetTeamNumber() )
	{
		if( ( d->GetNumCapturePoints( _sender->GetTeamNumber() ) > 1 &&
			  ScoreDifference( _sender ) > -15 ) ||
			 d->GetNumCapturePoints( _sender->GetTeamNumber() ) == 3 )
		{
			_sender->GetFSM()->ChangeState( Defend::GetInstance() );
		}
		else
		{
			_sender->GetFSM()->ChangeState( CaptureDomPoint::GetInstance() );
		}
	}
	else if( NumTeammatesHeadingToDP( _sender, _sender->GetCurrentDPTarget() ) > 1 )
	{
		_sender->GetFSM()->ChangeState( CaptureDomPoint::GetInstance() );
	}

	cpu.mark();

	
}

void CaptureDomPoint::Exit( Bot *_sender )
{
	_sender->behaviours.SetFollowPath( false );
}

// Will find the index of the nearest unmanned or enemy 
// controlled domination point
// Returns -1 if there are no suitable domination points
int CaptureDomPoint::GetDomPoint( Bot *_sender )
{
	DynamicObjects *d = DynamicObjects::GetInstance();
	int ret = -1;
	int nearest = -1;
	std::vector<int> availableDoms;

	for( int i = 0; i < NUMDOMINATIONPOINTS; i++ )
	{
		if( d->GetDominationPoint(i).m_OwnerTeamNumber != _sender->GetTeamNumber() )
		{
			availableDoms.push_back( i );

			if( nearest == -1 )
			{
				nearest = i;
			}
			else if( ( _sender->GetLocation() - d->GetDominationPoint(i).m_Location ).magnitude() <
				     ( _sender->GetLocation() - d->GetDominationPoint( nearest ).m_Location ).magnitude() )
			{
				nearest = i;
			}
		}
	}

	// Safety first...
	if( availableDoms.size() < 1 )
		return -1;

	// If there's just the one point, head for that
	if( availableDoms.size() == 1 )
		return availableDoms[0];


	// At this point, we know there's multiple points.  Need to decide.
	// Nearest would be best choice, see if anyone else is going there
	if( NumTeammatesHeadingToDP( _sender, nearest ) < 2 )
	{
		return nearest;
	}


	// At this point, more than one person is heading to the nearest
	// dom point to you.  Time to reassess
	

	// Let's see if there's another point we can go to that isn't
	// being headed to by teammates
	for( int i=0; i < availableDoms.size(); i++ )
	{
		// Ignore nearest - we've assessed that
		if( i == nearest )
			continue;

		if( DPIsUnmanned( _sender, availableDoms[i] ) )
			return availableDoms[i];

		if( NumTeammatesHeadingToDP( _sender, availableDoms[i] ) < 2 )
			return availableDoms[i];
	}

	// We've covered all points, really.  If you get to here, you might
	// as well head for the nearest
	return nearest;
}

bool CaptureDomPoint::EnemyNearby( Bot *_sender )
{
	int enemyTeamNo = 1 - _sender->GetTeamNumber();
	int ret = -1;

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		Bot &enemy = DynamicObjects::GetInstance()->GetBot( enemyTeamNo, i );

		if( StaticMap::GetInstance()->IsLineOfSight( _sender->GetLocation(), enemy.GetLocation() ) &&
			( _sender->GetLocation() - enemy.GetLocation() ).magnitude() < ATTACK_RANGE && 
			enemy.IsAlive() )
		{
			ret = i;
		}
	}

	if( ret != -1 )
	{
		_sender->SetNearestEnemy( ret );
		return true;
	}

	return false;
}

bool CaptureDomPoint::TeammateNearby( Bot *_sender )
{
	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		if( _sender->GetBotNumber() == i )
			continue;

		Bot &tm = DynamicObjects::GetInstance()->GetBot( _sender->GetTeamNumber(), i );

		if( StaticMap::GetInstance()->IsLineOfSight( _sender->GetLocation(), tm.GetLocation() ) &&
			( _sender->GetLocation() - tm.GetLocation() ).magnitude() < ATTACK_RANGE && 
			tm.IsAlive() )
		{
			return true;
		}
	}

	return false;
}

// Returns number of other friendlies headed to a DP and in CDP state
int CaptureDomPoint::NumTeammatesHeadingToDP( Bot *_sender, int _dp )
{
	int count = 0;

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		if( _sender->GetBotNumber() == i )
			continue;

		if( DynamicObjects::GetInstance()->GetBot( _sender->GetTeamNumber(), i ).GetCurrentDPTarget() == _dp ) //&&
			//DynamicObjects::GetInstance()->GetBot( _sender->GetTeamNumber(), i ).GetFSM()->IsInState( CaptureDomPoint::GetInstance() ) )
			count++;
	}
	return count;
}

int CaptureDomPoint::ScoreDifference( Bot *_sender )
{
	DynamicObjects *d = DynamicObjects::GetInstance();

	return ( d->GetScore( _sender->GetTeamNumber() ) - d->GetScore( 1 - _sender->GetTeamNumber() ) );
}

int CaptureDomPoint::EnemiesNearDP( Bot *_sender, int _dp )
{
	DynamicObjects *d = DynamicObjects::GetInstance();
	int enemyTeamNo = 1 - _sender->GetTeamNumber();
	int count = 0;

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		if ( ( d->GetBot( enemyTeamNo, i ).GetLocation() - d->GetDominationPoint(_dp).m_Location).magnitude() < 100 &&
			   d->GetBot( enemyTeamNo, i ).IsAlive() )
			count++;
	}

	return count;
}

bool CaptureDomPoint::DPIsUnmanned( Bot *_sender, int _dp )
{
	if( EnemiesNearDP( _sender, _dp ) == 0 )
		return true;

	return false;
}

// Attack

Attack* Attack::m_instance = NULL;

Attack::Attack()
{

}

Attack::~Attack()
{

}

Attack* Attack::GetInstance()
{
	if( m_instance == NULL )
		m_instance = new Attack();

	return m_instance;
}

void Attack::Release()
{
	if( m_instance )
	{
		delete m_instance;
		m_instance = NULL;
	}
}

void Attack::Enter( Bot *_sender )
{
	// Set Behaviours On
	_sender->behaviours.ClearPath();
	_sender->behaviours.AllOff();

	_sender->behaviours.SetWallAvoid( true );

	int enemyTeamNo = 1 - _sender->GetTeamNumber();

	_sender->SetTarget( enemyTeamNo, _sender->GetNearestEnemy() );
}

void Attack::Execute( Bot *_sender )
{
	int enemyTeamNo = 1 - _sender->GetTeamNumber();


	if( _sender->IsAlive() )
	{
		if( _sender->GetHealth() < 50 && 
			! ( TeamNearby( _sender ) ))
		{
			_sender->StopAiming();
			_sender->GetFSM()->ChangeState( Flee::GetInstance() );
		}

		if( CurrentEnemyOutOfRange( _sender ) )
		{
			_sender->StopAiming();

			// Check for any other surrounding enemies.
			// If there is one, sender's GetNearestEnemy is updated
			if( ! EnemyNearby( _sender ) )
			{
				_sender->GetFSM()->RevertState();
			}
			else
			{
				_sender->GetFSM()->ChangeState( Attack::GetInstance() );
			}
		}

		if( _sender->GetAccuracy() > 0.4 )
		{
			_sender->Shoot();
		}

		if( ! ( DynamicObjects::GetInstance()->GetBot( enemyTeamNo, _sender->GetNearestEnemy() ).IsAlive() ) )
		{
			if( EnemyNearby( _sender ) )
			{
				_sender->StopAiming();
				_sender->GetFSM()->ChangeState( Attack::GetInstance() );
			}
			else
			{
				_sender->StopAiming();
				_sender->GetFSM()->RevertState();
			}
		}
	}
	else
	{
		_sender->StopAiming();

		_sender->GetFSM()->ChangeState( Spawn::GetInstance() );
	}
}

void Attack::Exit( Bot *_sender )
{
	_sender->behaviours.SetPursue( false );
	_sender->behaviours.SetWallAvoid( false );
	_sender->behaviours.AllOff();
}

bool Attack::EnemyNearby( Bot *_sender )
{
	int ret = -1;
	int enemyTeamNo = 1 - _sender->GetTeamNumber();

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		Bot &enemy = DynamicObjects::GetInstance()->GetBot( enemyTeamNo, i );

		if( StaticMap::GetInstance()->IsLineOfSight( _sender->GetLocation(), enemy.GetLocation() ) &&
			( _sender->GetLocation() - enemy.GetLocation() ).magnitude() < ATTACK_RANGE && 
			enemy.IsAlive() )
		{
			ret = i;
		}
	}

	if( ret != -1 )
	{
		_sender->SetNearestEnemy( ret );
		return true;
	}

	return false;
}

bool Attack::CurrentEnemyOutOfRange( Bot *_sender )
{
	DynamicObjects *d = DynamicObjects::GetInstance();
	int enemyTeamNo = 1 - _sender->GetTeamNumber();

	if( (  _sender->GetLocation() - d->GetBot( enemyTeamNo, _sender->GetNearestEnemy() ).GetLocation() ).magnitude() > ATTACK_RANGE ||
		!( StaticMap::GetInstance()->IsLineOfSight( _sender->GetLocation(), d->GetBot( enemyTeamNo, _sender->GetNearestEnemy() ).GetLocation() ) ) )
	{
		return true;
	}

	return false;
}

bool Attack::TeamAttackingSameBot( Bot *_sender )
{
	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		if( _sender->GetTargetBot() == DynamicObjects::GetInstance()->GetBot( _sender->GetTeamNumber(), i ).GetTargetBot() &&
			i != _sender->GetBotNumber() && 
			DynamicObjects::GetInstance()->GetBot( _sender->GetTeamNumber(), i ).IsAlive() )
		{
			// Someone else on the team is also targeting this bot...
			return true;
		}
	}

	return false;
}

bool Attack::TeamNearby( Bot *_sender )
{
	DynamicObjects *d = DynamicObjects::GetInstance();

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		if( i == _sender->GetBotNumber() )
			continue;

		if( ( _sender->GetLocation() - d->GetBot( _sender->GetTeamNumber(), i ).GetLocation() ).magnitude() < 200.0 )
		{
			return true;
		}
	}

	return false;
}


// Defend
Defend* Defend::m_instance = NULL;

Defend::Defend()
{

}

Defend::~Defend()
{

}

Defend* Defend::GetInstance()
{
	if( m_instance == NULL )
		m_instance = new Defend();

	return m_instance;
}

void Defend::Release()
{
	if( m_instance )
	{
		delete m_instance;
		m_instance = NULL;
	}
}

void Defend::Enter( Bot *_sender )
{
	_sender->behaviours.AllOff();
	_sender->behaviours.SetWallAvoid( true );
	_sender->behaviours.SetFollowPath( true );

	int target = GetNearestDPToDefend( _sender );

	int xOffset = rand()%200 - 100;
	int yOffset = rand()%200 - 100;

	Vector2D targetLocation = DynamicObjects::GetInstance()->GetDominationPoint( target ).m_Location;
	targetLocation.XValue += xOffset;
	targetLocation.YValue += yOffset;

	_sender->SetCurrentDPTarget( target );
	_sender->behaviours.SetTarget( targetLocation );
	_sender->behaviours.SetPath( targetLocation );
}

void Defend::Execute( Bot *_sender )
{
	if( EnemyNearby( _sender ) )
	{
		_sender->GetFSM()->ChangeState( Attack::GetInstance() );
	}

	if ( TeammatesNearDP( _sender, _sender->GetCurrentDPTarget() ) > 1 ) 
	{
		_sender->GetFSM()->ChangeState( Defend::GetInstance() );
	}

	// If only one point left or the point you're heading to is taken over by an enemy
	if( DynamicObjects::GetInstance()->GetNumCapturePoints( _sender->GetTeamNumber() ) < 2 ||
	    DynamicObjects::GetInstance()->GetDominationPoint( _sender->GetCurrentDPTarget() ).m_OwnerTeamNumber != _sender->GetTeamNumber() )
	{
		_sender->GetFSM()->ChangeState( CaptureDomPoint::GetInstance() );
	}

	// This will make it look like they're patrolling a dom point
	if( ( _sender->GetLocation() - _sender->behaviours.GetTarget() ).magnitude() < 10.0 )
	{
		_sender->GetFSM()->ChangeState( Defend::GetInstance() );
	}
}

void Defend::Exit( Bot *_sender )
{
	// _sender->SetCurrentDPTarget( -1 );
}

int Defend::TeammatesNearDP( Bot *_sender, int _dp )
{
	int count = 0;
	DynamicObjects *d = DynamicObjects::GetInstance();

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		// Ignore self
		if( i == _sender->GetBotNumber() )
			continue;

		if( ( d->GetBot( _sender->GetTeamNumber(), i ).GetLocation() - d->GetDominationPoint( _dp ).m_Location ).magnitude() < 100 )

			count++;
	}

	return count;
}

int Defend::GetNearestDPToDefend( Bot *_sender )
{
	std::vector<int> dpShortlist;
	int nearest = rand()%3;

	for( int i=0; i < NUMDOMINATIONPOINTS; i++ )
	{
		if( DynamicObjects::GetInstance()->GetDominationPoint(i).m_OwnerTeamNumber == _sender->GetTeamNumber() )
		{
			dpShortlist.push_back( i );
			
			if( dpShortlist.size() == 1 )
			{
				nearest = i;
			}
			else if ( ( _sender->GetLocation() - DynamicObjects::GetInstance()->GetDominationPoint(i).m_Location ).magnitude() <
					  ( _sender->GetLocation() - DynamicObjects::GetInstance()->GetDominationPoint(nearest).m_Location ).magnitude() )
			{
				nearest = i;
			}
		}
	}

	// This should never really happen but it's just for safety
	if( dpShortlist.size() < 1 )
	{
		return rand()%3;
	}

	// This shouldn't really happen either - if there's only one captured
	// point then there's no point defending
	if( dpShortlist.size() == 1 )
	{
		return dpShortlist[0];
	}

	// If there's one or less teammates at the nearest DP, go defend that
	if( TeammatesNearDP( _sender, nearest ) < 2 &&
		TeammatesHeadingToDP( _sender, nearest ) < 2 )
	{
		return nearest;
	}

	// At this point, we know we have more than one potential DP
	for( int i = 0; i < dpShortlist.size(); i++ )
	{
		if( i == nearest )
			continue;

		if( TeammatesNearDP( _sender, dpShortlist[i] ) < 2 )
			return dpShortlist[i];
	}

	// If we've got to hear, all points are pretty much covered. 
	// Just head to the nearest

	return nearest;
}

int Defend::TeammatesHeadingToDP( Bot *_sender, int _dp )
{
	int count = 0;

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		if( i == _sender->GetBotNumber() )
			continue;

		if( DynamicObjects::GetInstance()->GetBot( _sender->GetTeamNumber(), i ).GetCurrentDPTarget() == _dp &&
			DynamicObjects::GetInstance()->GetBot( _sender->GetTeamNumber(), i ).GetFSM()->IsInState( Defend::GetInstance() ) )
			count++;
	}

	return count;
}

bool Defend::EnemyNearby( Bot *_sender )
{
	int enemyTeamNo = 1 - _sender->GetTeamNumber();
	int ret = -1;

	for( int i=0; i < NUMBOTSPERTEAM; i++ )
	{
		Bot &enemy = DynamicObjects::GetInstance()->GetBot( enemyTeamNo, i );

		if( StaticMap::GetInstance()->IsLineOfSight( _sender->GetLocation(), enemy.GetLocation() ) &&
			( _sender->GetLocation() - enemy.GetLocation() ).magnitude() < ATTACK_RANGE && 
			enemy.IsAlive() )
		{
			ret = i;
		}
	}

	if( ret != -1 )
	{
		_sender->SetNearestEnemy( ret );
		return true;
	}

	return false;
}


// Spawn state

Spawn* Spawn::m_instance = NULL;

Spawn::Spawn()
{

}

Spawn::~Spawn()
{

}

Spawn* Spawn::GetInstance()
{
	if( m_instance == NULL )
		m_instance = new Spawn();

	return m_instance;
}

void Spawn::Release()
{
	if( m_instance )
	{
		delete m_instance;
		m_instance = NULL;
	}
}

void Spawn::Enter( Bot *_sender )
{
	Vector2D temp = StaticMap::GetInstance()->GetSpawnPoint( _sender->GetTeamNumber() );
	int random;

	// Slightly randomise inital location - this will spread them out a bit
	if( _sender->GetTeamNumber() == 0 )
		temp.XValue = 50;
	else
		temp.XValue = 950;

	random = rand()%100 - 50;

	temp.YValue += random;

	_sender->behaviours.SetTarget( temp );

	_sender->behaviours.SetSeek( true );
	_sender->behaviours.SetWallAvoid( true );
}

void Spawn::Execute( Bot *_sender )
{
	GameTimer cpu;

	cpu.mark();

	if( ( _sender->GetLocation() - _sender->behaviours.GetTarget() ).magnitude() < 2 )
	{
		if( DynamicObjects::GetInstance()->GetNumCapturePoints( _sender->GetTeamNumber() ) > 1
			&& ScoreDifference( _sender ) > -15 )
		{
			_sender->GetFSM()->ChangeState( Defend::GetInstance() );
		}
		else
		{
			_sender->GetFSM()->ChangeState( CaptureDomPoint::GetInstance() );
		}
	}

	cpu.mark();

	//if( _sender->GetTeamNumber() == 0 &&
	//	_sender->GetBotNumber() == 0 )
	//{
	//	ErrorLogger::Writeln( cpu.mdFrameTime );
	//	Renderer::GetInstance()->DrawNumberAt( Vector2D( 700, 700 ), cpu.mdFrameTime );
	//}
}

void Spawn::Exit( Bot *_sender )
{
	_sender->behaviours.AllOff();
}

int Spawn::ScoreDifference( Bot *_sender )
{
	DynamicObjects *d = DynamicObjects::GetInstance();

	return ( d->GetScore( _sender->GetTeamNumber() ) - d->GetScore( 1 - _sender->GetTeamNumber() ) );
}
