// Behaviours class header
// Ian Hutchinson
// 09/11/2012
#pragma once

#include "vector2D.h"
#include "rules.h"
#include "mydrawengine.h" // Temporary for drawing sensors
#include "renderer.h"
#include "staticmap.h"
#include <vector>

class Bot;

class Behaviours
{
private:
	// Switches
	bool m_seekOn;
	bool m_fleeOn;
	bool m_arriveOn;
	bool m_pursueOn;
	bool m_evadeOn;
	bool m_wallAvoidOn;
	bool m_followPathOn;


	// Owning bot
	Bot* m_owner;

	// Multipliers
	double m_seekMulti;
	double m_fleeMulti;
	double m_arriveMulti;
	double m_pursueMulti;
	double m_evadeMulti;
	double m_wallMulti;
	double m_followMulti;


	// Vectors
	std::vector<Vector2D> m_currentPath;
	Vector2D m_totalForce;
	Vector2D m_target;
	Vector2D m_targetVel;

public:
	Behaviours();
	~Behaviours();

	void SetOwner( Bot* _owner );

	Vector2D Seek( Vector2D _target );
	Vector2D Flee( Vector2D _target );
	Vector2D Arrive( Vector2D _target );
	Vector2D Pursue( Vector2D _target, Vector2D _targetVel );
	Vector2D Evade( Vector2D _target, Vector2D _targetVel );
	Vector2D WallAvoid();
	Vector2D FollowPath();
	void SetPath( Vector2D _target );
	Vector2D AccumulateBehaviours();
	void SetSeek( bool _status );
	void SetFlee( bool _status );
	void SetArrive( bool _status );
	void SetPursue( bool _status );
	void SetEvade( bool _status );
	void SetWallAvoid( bool _status );
	void SetFollowPath( bool _status );
	void SetTarget( Vector2D _target );
	void SetTargetVelocity( Vector2D _vel );
	bool AccumulateForce( Vector2D &_runningTotal, Vector2D _forceToAdd );
	void DrawPath();
	void ClearPath();
	void WriteTotalForce() const;
	void AllOff();
	Vector2D GetTarget() const;
};