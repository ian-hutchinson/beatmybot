// Node.cpp

#include "Node.h"

double Edge::GetCost() const
{
	return m_cost;
}

int Edge::GetToIndex() const
{
	return m_toIndex;
}

int Edge::GetFromIndex() const
{
	return m_fromIndex;
}

void Node::SetLocation( Vector2D _loc )
{
	m_Location = _loc;
}

Vector2D Node::GetLocation() const
{
	return m_Location;
}

void Node::SetIndex( int _idx )
{
	m_indexNumber = _idx;
}

int Node::GetIndex() const
{ 
	return m_indexNumber;
}

void Node::AddEdge(Edge _edge)
{
	m_edgeList.push_back( _edge );
}

int Node::GetListEntries() const
{
	return m_edgeList.size();
}

double Node::GetCostAtIndex(int _idx) const
{
	return m_edgeList[_idx].GetCost();
}

int Node::GetToIndex(int _idx) const
{
	return m_edgeList[_idx].GetToIndex();
}

int Node::GetFromIndex(int _idx) const
{
	return m_edgeList[_idx].GetFromIndex();
}

void Node::Reset()
{
	m_fScore = 0;
	m_gScore = 0;
	m_prevNode = -1;
	m_nodeStatus = UNEXPLORED;
}

Node::Node()
{
	Reset();
}

void Node::SetStatus( NodeStatus _newStatus )
{
	m_nodeStatus = _newStatus;
}

NodeStatus Node::GetStatus()
{
	return m_nodeStatus;
}

void Node::SetGScore( double _newGScore )
{
	m_gScore = _newGScore;
}

double Node::GetGScore() const
{
	return m_gScore;
}

void Node::SetPreviousNode( int _prevNode )
{
	m_prevNode = _prevNode;;
}

int Node::GetPreviousNode() const
{
	return m_prevNode;
}

void Node::CalculateFScore()
{
	m_fScore = m_gScore + m_hScore;
}

double Node::GetFScore() const
{
	return m_fScore;
}

void Node::SetHScore( double _newHScore )
{
	m_hScore = _newHScore;
}

double Node::GetHScore() const
{
	return m_hScore;
}