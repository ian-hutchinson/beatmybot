// Node.h

#include "vector2D.h"
#include <vector>

enum NodeStatus { OPEN, CLOSED, UNEXPLORED };

class Edge
{
private:
	int m_fromIndex;
	int m_toIndex;
	double m_cost;
public:
	Edge( int _from, int _to, double _cost ):m_fromIndex( _from ), 
											 m_toIndex( _to ), 
											 m_cost( _cost ){};

	double GetCost() const;
	int GetToIndex() const;
	int GetFromIndex() const;
};


class Node
{
private:
	Vector2D m_Location;
	int m_indexNumber;
	std::vector<Edge> m_edgeList;

	double m_gScore;
	double m_fScore;
	double m_hScore;

	int    m_prevNode;

	NodeStatus m_nodeStatus;
public:
	Node();

	void SetLocation( Vector2D _loc );
	void SetIndex( int _idx );
	void AddEdge( Edge _edge );

	Vector2D GetLocation() const;
	int GetIndex() const;
	int GetListEntries() const;
	double GetCostAtIndex( int _idx ) const;
	int GetToIndex( int _idx ) const;
	int GetFromIndex( int _idx ) const;
	void SetStatus( NodeStatus _newStatus );
	NodeStatus GetStatus();
	void Reset();
	void SetGScore( double _newGScore );
	double GetGScore() const;
	void CalculateFScore();
	double GetFScore() const;
	void SetHScore( double _newHScore );
	double GetHScore() const;
	void SetPreviousNode( int _prevNode );
	int  GetPreviousNode() const;
};