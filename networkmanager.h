// networkmanager.h
// Network manager header file (server version)
// Ian Hutchinson
#ifndef __NETMAN_H
#define __NETMAN_H

#include <windows.h>
#include <process.h>
#include <WinSock.h>
#include "errorlogger.h"
#include "errortype.h"
#include "rules.h"

const int MAX_CLIENTS = 4;

struct ClientThreadArgs
{
	int clnt_sock;  // Client socket
};

struct BotData
{
	double x, y;
	double respawn;
	double direction;
	double tX, tY;
	bool shooting;
	int damage;
};

struct TeamData
{
	int score;
	BotData bots[NUMBOTSPERTEAM];
};

struct dpData
{
	int owner;
};

struct SendData
{
	dpData doms[NUMDOMINATIONPOINTS];
	TeamData teams[NUMTEAMS];
};



class NetworkManager
{
private:
	static NetworkManager *instance;

	static int sock;
	static bool server_up;
	static int clients_connected;
	static ClientThreadArgs *cthread_args;

	unsigned short port;
	WSADATA wsa_data;
	ErrorType Release();
	NetworkManager( unsigned short );
	~NetworkManager();

public:
	static NetworkManager* GetInstance();
	static ErrorType CreateTCPServerSocket( unsigned short );
	static void* AcceptTCPConnection( void* );
	static ErrorType HandleTCPClient( int );
	static void* HandleClientThread( void* );
	static NetworkManager* Start( unsigned short );
    static ErrorType Terminate();
	ErrorType Update();
	int GetNumClientConnected();
	void UpdateData( SendData d );
};


#endif