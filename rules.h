#pragma once

const double MAXBOTSPEED = 100.0;
const double TIMEBETWEENSHOTS = 0.5;
const double RESPAWNTIME = 10.0;
const int NUMTEAMS = 2;
const int NUMBOTSPERTEAM = 4;
const int NUMDOMINATIONPOINTS = 3;
const double DOMINATIONRANGE = 20.0;		// Range from a domination point to control it.
const double MAXIMUMACCELERATION = 60.0;
const double MAXIMUMSPEED = 100.0;
const double ACCURATERANGE = 100;			// Range at which you can expect 100% accuracy
const double AIMRATE = 0.3;					// Rate at which accuracy increases
const double MAXIMUMFORCE = 250.0;