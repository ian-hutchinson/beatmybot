#pragma once
#include "errortype.h"
#include "bot.h"
#include "networkmanager.h"
#include "renderer.h"

const int MAXTEAMS = 4;
const int MAXBOTSPERTEAM = 16;
const int MAXDOMINATIONPOINTS=16;



struct DominationPoint
{
	int m_OwnerTeamNumber;
	Vector2D m_Location;

	DominationPoint()
	{
		m_OwnerTeamNumber=-1;
	}
};

struct Team
{
	Bot m_rgBots[MAXBOTSPERTEAM];
	int m_iScore;

	void Reset()
	{
		m_iScore = 0;
	}

	Team()
	{
		Reset();
	}
};


class DynamicObjects
{
private:
	DynamicObjects();
	~DynamicObjects();
	DynamicObjects(const DynamicObjects & other);	//Disable
	Team m_rgTeams[MAXTEAMS];
	DominationPoint m_rgDominationPoints[MAXDOMINATIONPOINTS];
	static DynamicObjects* pInst;
	int m_iNumPlacedDominationPoints;
	double m_dNextScorePoint;						// Time until next points get added
public:
	static DynamicObjects* GetInstance();					// Thread-safe singleton
	static void Release();
	ErrorType Update(double frametime);
	ErrorType Render();
	void Initialise();
	Bot& GetBot(int teamNumber, int botNumber);
	int GetScore(int teamNumber);
	DominationPoint GetDominationPoint(int pointNumber);
	void PlaceDominationPoint(Vector2D location);
	int GetNumPlacedDomPoints() const;
	int GetNumCapturePoints( int teamNumber ) const;
	void PackData( SendData &sd );
};