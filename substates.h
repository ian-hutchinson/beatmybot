#ifndef __SUBSTATES_H__
#define __SUBSTATES_H__

#include "state.h"
#include "bot.h"
#include "dynamicobjects.h"
#include "staticmap.h"
#include "rules.h"

class Flee: public State<Bot>
{
private:
	Flee();
	~Flee();
	Flee( const Flee &other );
	static Flee *m_instance;
	Bot *m_enemy;
public:
	static Flee* GetInstance();
	static void Release();
	bool Escaped( Bot *_sender );
	bool NobodyAimingAtMe( Bot *_sender );
	void Enter( Bot *_sender );
	void Exit( Bot *_sender );
	void Execute( Bot *_sender );
};

class CaptureDomPoint: public State<Bot>
{
private:
	CaptureDomPoint();
	~CaptureDomPoint();
	CaptureDomPoint( const CaptureDomPoint &other );
	static CaptureDomPoint *m_instance;
public:
	static CaptureDomPoint* GetInstance();
	static void Release();
	int GetDomPoint( Bot *_sender );
	int NumTeammatesHeadingToDP( Bot *_sender, int _dp );
	int ScoreDifference( Bot *_sender );
	int EnemiesNearDP( Bot *_sender, int _dp );
	bool DPIsUnmanned( Bot *_sender, int _dp );
	bool EnemyNearby( Bot *_sender );
	bool TeammateNearby( Bot *_sender );
	void Enter( Bot *_sender );
	void Exit( Bot *_sender );
	void Execute( Bot *_sender );
};

class Attack: public State<Bot>
{
private:
	Attack();
	~Attack();
	Attack( const Attack &other );
	static Attack *m_instance;
public:
	static Attack* GetInstance();
	static void Release();
	bool EnemyNearby( Bot *_sender );
	bool CurrentEnemyOutOfRange( Bot *_sender );
	bool TeamAttackingSameBot( Bot *_sender );
	bool TeamNearby( Bot *_sender );
	void Enter( Bot *_sender );
	void Execute( Bot *_sender );
	void Exit( Bot *_sender );
};

class Defend: public State<Bot>
{
private:
	Defend();
	~Defend();
	Defend( const Defend &other );
	static Defend *m_instance;
public:
	static Defend* GetInstance();
	static void Release();
	int TeammatesNearDP( Bot *_sender, int _dp );
	int TeammatesHeadingToDP( Bot *_sender, int _dp );
	int GetNearestDPToDefend( Bot *_sender );
	bool EnemyNearby( Bot *_sender );
	void Enter( Bot *_sender );
	void Execute( Bot *_sender );
	void Exit( Bot *_sender );
};

//class Chase: public State<Bot>
//{
//private:
//	Chase();
//	~Chase();
//	Chase( const Chase &other );
//	static Chase *m_instance;
//public:
//	static Chase* GetInstance();
//	static void Release();
//	void Enter( Bot *_sender );
//	void Execute( Bot *_sender );
//	void Exit( Bot *_sender );
//};

class Spawn: public State<Bot>
{
private:
	Spawn();
	~Spawn();
	Spawn (const Spawn &other );
	static Spawn *m_instance;
public:
	static Spawn* GetInstance();
	static void Release();
	int ScoreDifference( Bot *_sender );
	void Enter( Bot *_sender );
	void Execute( Bot *_sender );
	void Exit( Bot *_sender );
};

#endif